

TARGET   = pythonModule
TEMPLATE = app

DESTDIR           = ../../lib

mac:CONFIG-= app_bundle

include ( ../../PythonQt3.0/build/common.prf )
include ( ../../PythonQt3.0/build/PythonQt.prf )
include ( ../../PythonQt3.0/build/PythonQt_QtAll.prf )

contains(QT_MAJOR_VERSION, 5) {
  QT += widgets
}

HEADERS +=                    \
  src/PyExampleObject.h
  
SOURCES +=                    \
  src/PyExampleObject.cpp         \
  src/main.cpp

RESOURCES += src/pythonModule.qrc

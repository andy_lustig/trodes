#include "mainWindow.h"
#include "ui_mainWindow.h"
#include "trodesDataInterface.h"


MainWindow::MainWindow(QStringList arguments, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->statusbar->showMessage("Processing arguments.");

    QString serverAddress, serverPort, configFilename;
    int optionInd = 1;
    while (optionInd < arguments.length()) {
        qDebug() << "Option: " << arguments.at(optionInd);
        if (arguments.length() > optionInd+1 )
          qDebug() << "Option(+1): " << arguments.at(optionInd+1);
        if ((arguments.at(optionInd).compare("-serverAddress",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            serverAddress = arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-serverPort",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            serverPort = arguments.at(optionInd+1);
            optionInd++;
        }
        else if ((arguments.at(optionInd).compare("-trodesConfig",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            configFilename = arguments.at(optionInd+1);
            optionInd++;
        }
        optionInd++;
    }

    ui->statusbar->showMessage("Establishing Trodes interface.");

    TrodesMessageInterface *trodesMessages = new TrodesMessageInterface(configFilename, serverAddress, serverPort);
    QThread *msgThread = new QThread();
    trodesMessages->moveToThread(msgThread);
    connect(msgThread, &QThread::started, trodesMessages, &TrodesMessageInterface::setup);
    connect(trodesMessages, &TrodesMessageInterface::quitReceived, this, &MainWindow::close);
    connect(trodesMessages, &TrodesMessageInterface::statusMessage, this, &MainWindow::changeStatusBarMessage);
    connect(trodesMessages, &TrodesMessageInterface::newTimestamp, this, &MainWindow::changeTimestamp);
    connect(trodesMessages, &TrodesMessageInterface::newContinuousMessage, this, &MainWindow::changeContinuousMessage);
    msgThread->start();

    changeTimestampLabel(ui->continuousClock, 0);
    changeTimestampLabel(ui->blockContClock, 0);
    changeTimestampLabel(ui->digitalIOClock, 0);
    changeTimestampLabel(ui->positionClock, 0);
    changeTimestampLabel(ui->spikesClock, 0);

    /* ***ABOUT MENU*** */
    menuHelp = new QMenu;
    menuHelp->setTitle("Help");
    actionAbout = new QAction(this);
    actionAbout->setText("About");
    actionAbout->setMenuRole(QAction::AboutRole);
    menuHelp->addAction(actionAbout);
    menuBar()->addAction(menuHelp->menuAction());
    connect(actionAbout, SIGNAL(triggered()), this, SLOT(about()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeTimestampLabel(QLabel *widget, quint32 time)
{
    uint seconds, minutes, hours;
    if (hardwareConf == NULL) {
        seconds = 0; minutes = 0; hours = 0;
    }
    else {
        seconds = time / (hardwareConf->sourceSamplingRate);
        minutes = (seconds / (60));
        hours = minutes / 60;
        minutes = minutes % 60;
        seconds = seconds % 60;
    }
    widget->setText(QString("%1:%2:%3").arg(hours)
                   .arg((uint)minutes,2,10,QChar('0'))
                   .arg((uint)seconds,2,10,QChar('0')));
}

void MainWindow::changeStatusBarMessage(QString msg)
{
    ui->statusbar->showMessage(msg);
}

void MainWindow::changeTimestamp(quint8 dataType, quint32 time)
{
    switch (dataType) {
    case TRODESDATATYPE_CONTINUOUS:
        changeTimestampLabel(ui->continuousClock, time);
        break;
    case TRODESDATATYPE_DIGITALIO:
        changeTimestampLabel(ui->digitalIOClock, time);
        break;
    case TRODESDATATYPE_POSITION:
        changeTimestampLabel(ui->positionClock, time);
        break;
    case TRODESDATATYPE_BLOCK_CONTINUOUS:
        changeTimestampLabel(ui->blockContClock, time);
        break;
    case TRODESDATATYPE_SPIKES:
        changeTimestampLabel(ui->spikesClock, time);
        break;
    }
}

void MainWindow::changeContinuousMessage(QString msg)
{
    ui->continuousMsg->setText(msg);
}

void MainWindow::about() {
    QMessageBox::about(this, tr("About SimpleCommunicator"), tr(qPrintable(GlobalConfiguration::getVersionInfo())));
}

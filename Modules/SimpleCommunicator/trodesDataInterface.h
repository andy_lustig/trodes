#ifndef TRODESDATAINTERFACE_H
#define TRODESDATAINTERFACE_H

/* ***** USAGE INSRUCTIONS *****
 * ---- Running SimpleCommunicator ----
 * This module can ONLY be started up by opening a config file which includes the line
 * "<SingleModuleConfiguration moduleName="SimpleCommunicator" sendNetworkInfo="1" sendTrodesConfig="1"/>"
 * otherwise the module will crash.
 * ---- Requesting and Processing New Datatypes ----
 * 1) Append a new TRODESDATATYPE onto the trodesModuleInterface->dataNeeded list
 * 2) Set up a new client socket in TrodesDataInterface::connectDataClient by adding the new TRODESDATATYPE
 *    to the if-statement, setting up the client connection in the same manner as the other datatypes are
 *    set up.  Be sure to call tc->turnOnDataStreaming() to begin the data stream.
 * 3) Set up the new TRODESDATATYPE's data processing method in TrodesDataInterface::processMessage by adding
 *    the new data type to the if statment.  Decode the incomming message stream via the call, "msgStream >>
 *    var", according to the data structure of the message.  Finally, make sure to emit the receieved data
 *    as a signal once you have extracted everything from the msgStream.
 * 4) Relay the received data from step 3 from through the TrodesMessageInterface in TrodesMessageInterface::
 *    setup() by connecting the signals emitted in step 3 to corresponding TrodesMessageInterface signals (for
 *    simplicity, the signals can be identical).  The TrodesDataInterface object is declared "dataInterface,"
 *    and the TrodesMessageInterface object is declared "this," connect only if connectionSuccess is true,
 *    lest you segfault.
 * 5) Connect the signals emitted in step 4 from the trodesMessageInterface object to mainWindow application
 *    object in the MainWindow constructor in mainWindow.cpp.  Make sure you have created the appropriate
 *    MainWindow slot functions to receive the incomming data.  The TrodesMessageInterface object is declared
 *    "trodesMessages," and the MainWindow object as "this."
 * 6) Data from another module should now be available in the mainWindow of the receiving application via the
 *    slots connected in step 5, do with it what you will.
 */
//NOTE: for now continuous data defaults to tcp connections.

//#include <chrono>
#include <QObject>
#include "trodesSocket.h"
#include <errno.h>

#ifndef WIN32
#define NATIVE_SOCKETS
#endif

#ifdef NATIVE_SOCKETS
#include <unistd.h>
#include <cstring>
#include <sys/socket.h> /* for socket(), connect(), send(), recv(), sendto(), and recvfrom() */
#include <arpa/inet.h>  /* for sockaddr_in, inet_addr(), and htons() */
#include <string.h>     /* for memset() */
#endif

#define SHOW_LATENCY_TRACKING 0 //turns on latency and packet transfer loss rate tracking


/**
template <class T>
class avgCalculator {
public:
    avgCalculator() { sum = 0; min = -1; max = 0;};
    inline void insert(T obj) { data.append(obj); sum += obj; if (obj < min || min == -1) {min = obj;} if (obj > max) {max = obj;}};
    inline float average(void) { return(sum/data.length()); };
    inline T getMax(void) {return(max);}
    inline T getMin(void) {return(min);}

private:
    T sum;
    T min;
    T max;
    QVector<T> data;
};**/

class TrodesMessageInterface : public QObject
{
    Q_OBJECT
public:
    explicit TrodesMessageInterface(QString cfgFileName, QString srvAddr, QString srvPort, QObject *parent = 0);
    ~TrodesMessageInterface();

    TrodesModuleNetwork *trodesModuleInterface;
    QString configFileName;
    QString serverAddress;
    QString serverPort;

signals:
    void quitReceived();
    void statusMessage(QString);
    void newTimestamp(quint8 datatype, quint32 timestamp);
    void newContinuousMessage(QString);
    void newPosition(qint16 x, qint16 y, qint16 linSeg, qreal linPos);
    void newVelocity(qreal velocity);
    void timeRecieved(quint32 time);
    void numPacketsSent(int num);
    //MARK: EVENT
    void eventListReceived(QVector<TrodesEvent> evList);

public slots:
    void setup();
    void quitNow();

public:
    void Run();
    void timeRecevied(quint32);

private:
    quint32 currentTime;

};

class TrodesClientSocket : public QObject
{
    Q_OBJECT
public:
    explicit    TrodesClientSocket(quint8 dType, int uData, QTcpSocket *&sock, QObject *parent = 0);
    explicit    TrodesClientSocket(quint8 dType, int uData, QUdpSocket *&sock, QObject *parent = 0);
    explicit    TrodesClientSocket(quint8 dType, int uData, qint16 sockType, int sock, QObject *parent = 0);
    ~TrodesClientSocket();

    bool        waitForReadyRead(int msecs);

    qint16      socketType;
    QTcpSocket  *tcpSocket;
    QUdpSocket  *udpSocket;
    quint8      dataType;
    int         userData;

    int         nativeSock;
    QAbstractSocket::SocketState sockState;
public slots:
    void changeSockState(QAbstractSocket::SocketState state);
    void readSocket(void);
signals:
    void sig_readSocket(TrodesClientSocket* sock);

};



#ifdef NATIVE_SOCKETS

/*
   A TrodesNativeClient uses native socket calls for data communication

 */

class TrodesNativeClient : public QObject {
    Q_OBJECT

public:
    TrodesNativeClient(int sockType, QString hName, quint16 p, QObject *parent = 0);

    void connectToHost();
    bool isConnected();

    void setDataType(quint8 dataType, qint16 userData); //used to change the socket to a dedicated data line
    void setNTrodeId(int trodeId);
    void setNTrodeIndex(int index);
//    void setNTrodeChan(int trodeChan);
    void sendDecimationValue(quint16 dec);
    void turnOnDataStreaming();
    void turnOffDataStreaming();

    int getPort();
    QString getHostname();
    int getDescriptor();

private:
    int                 socketType; // TCPIP, UDP or LOCAL
    int                 sock;       // socket descriptor
    bool                connected;

    int                 socketDesc;  /* Socket descriptor */
    struct sockaddr_in  servAddr;    /* server address structure */
    QString             hostname;
    int                 port;

    int                 nTrodeId, nTrodeIndex;

    QAbstractSocket     *qSock;

    void sendMessage(quint8 dataType, QByteArray message);
    template<class T>
    void sendMessage(quint8 messageType, T message);

};

#endif



class TrodesDataInterface : public QObject
{
    Q_OBJECT
public:
    explicit TrodesDataInterface(QObject *parent = 0);
    ~TrodesDataInterface();

signals:
    void newTimestamp(quint8 datatype, quint32 timestamp);
    void newContinuousMessage(QString);
    void newPosition(qint16 x, qint16 y, qint16 linSeg, qreal linPos);
    void newVelocity(qreal velocity);
    void statusMessage(QString);
    void packetsSent(int num);

public slots:
    void connectDataClient(DataTypeSpec *da, int currentDataType);
    //void setupInterface(QUdpSocket *socket, qint16 userData)
//    void dataAvailableUpdated();
    void Run();
    void testSig(void) {qDebug() << "+++++++ ready to READ DATA ++++++++"; };
    void readFromSocket(TrodesClientSocket* sock);
    //void readFromSocket(int sType, QAbstractSocket *sock);


private:
    int packetsRecieved;
    qint16                  socketType;
    QList<TrodesClientSocket*> socketList;
    //QList<QTcpSocket*>      tcpSocketList;
    //QList<QUdpSocket*>      udpSocketList;
    //QList<quint8>           dataTypeList;
    //QList<int>              userDataList;

    QByteArray dataBuffer;
    TrodesDataStream inStream;

    quint64 continuousDataBlockSize;
    quint64 headerSize;
    quint64 continuousBufferSize;
    quint64 blockContBufferSize;
    quint64 positionDataBufferSize;
    quint64 spikeDataBufferSize;
    quint64 dioDataBufferSize;

    quint64 minDataBufferSize;
    quint64 maxDataBufferSize;


    QVector<int16_t> blockData;

    QVector<int16_t> continuousData;
//    QVector<uint32_t> continuousTimestamps;
    QVector<long> continuousTimestamps;
    qint32 t1;
    qint32 t2;
    avgCalculator<qint32> latencyAvg;
    avgCalculator<int> timeLatencyAvg;
    avgCalculator<float> timeLatencyAvg2;
    avgCalculator<int> spikeLatencyAvg;
   // clock_t sysClock;
    int sLatencyToken;
    int posStreamLatencyToken;
    //QList<QAbstractSocket::SocketState> stateList;

private:
    TrodesClient *createClient(qint16 socketType, QString hostname, quint16 tcpPort, quint16 udpPort, int dataType, int userData);

#ifdef NATIVE_SOCKETS
    TrodesNativeClient *createNativeClient(qint16 socketType, QString hostname, quint16 tcpPort, quint16 udpPort, int dataType, int userData);
#endif


    void udpDataMessageReceived(TrodesClientSocket *client);
    void tcpDataMessageReceived(TrodesClientSocket *client);
#ifdef NATIVE_SOCKETS
    void nativeUdpDataMessageReceived(TrodesClientSocket *client);
    void nativeTcpDataMessageReceived(TrodesClientSocket *client);
#endif
    void processMessage(quint8 messageType, quint32 messageSize, TrodesDataStream &msgStream, int userData);


#ifdef NATIVE_SOCKETS
    int maxfd;
    fd_set              sockSet;     /* Set of socket descriptors for select() */
#endif

};




#endif // TRODESDATAINTERFACE_H

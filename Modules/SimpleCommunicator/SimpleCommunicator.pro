include(../module_defaults.pri)
QT       += core gui network xml widgets

#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
#macx: {
#release:DESTDIR = ../../../bin/macOS
#debug:DESTDIR = ../../../bin/macOS
#}

#unix:!macx: {
#release:DESTDIR = ../../../bin/linux
#debug:DESTDIR = ../../../bin/linux
#}

#win32: {
#release:DESTDIR = ../../../bin/win32
#debug:DESTDIR = ../../../bin/win32
#}
##release:DESTDIR = ../../../bin
##debug:DESTDIR = ../../../bin

TARGET = SimpleCommunicator
TEMPLATE = app

#CONFIG += warn_on

#CONFIG += c++11

#QMAKE_CFLAGS_WARN_ON += -Wno-unused-parameter
#QMAKE_CXXFLAGS += -std=c++11
#QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter


INCLUDEPATH += ../../Trodes/src-config
INCLUDEPATH += ../../Trodes/src-main
#INCLUDEPATH +=../cameraModule/src

#GIT_COMMAND = git --git-dir $$system_quote($$system_path($$TRODES_REPO_DIR/.git)) \
#--work-tree $$system_quote($$system_path($$TRODES_REPO_DIR/)) describe --always --all \
#--tags --dirty --long

##must not contain spaces
#GIT_COMMIT = $$system($$GIT_COMMAND)
#DEFINES += GIT_COMMIT=\\\"$$GIT_COMMIT\\\"

SOURCES += main.cpp\
    ../../Trodes/src-main/trodesSocket.cpp \
    ../../Trodes/src-config/configuration.cpp \
    trodesDataInterface.cpp \
    mainWindow.cpp \
    ../../Trodes/src-main/trodesdatastructures.cpp \
    ../../Trodes/src-main/eventHandler.cpp
   # ../cameraModule/src/videoDisplay.cpp


HEADERS  += \
    ../../Trodes/src-main/trodesSocket.h \
    ../../Trodes/src-main/trodesSocketDefines.h \
    ../../Trodes/src-config/configuration.h \
    trodesDataInterface.h \
    mainWindow.h \
    ui_mainWindow.h \
    ../../Trodes/src-main/trodesdatastructures.h \
    ../../Trodes/src-main/eventHandler.h
    #../cameraModule/src/videoDisplay.h

FORMS += \
    mainwindow.ui 


/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SCRIPTEDITOR_H
#define SCRIPTEDITOR_H

#include <QTextEdit>
#include "highlighter.h"

class scriptEditor : public QTextEdit {
    Q_OBJECT
public:
    explicit scriptEditor(QWidget *parent = 0);
    ~scriptEditor();
    Highlighter* scriptHighlighter;
    void setData(const QByteArray &data);
    void removeData();
    void setFileName(QString);
    QString getFileName();
    bool isUnSaved();
    void trackIfSaved();
    void asHtml(QString& html);

public slots:
    void setUnsavedFile();
    void setSavedFile();

private:
    QString fileName;
    bool unsavedData;


protected:
    virtual void keyPressEvent(QKeyEvent *e);

signals:
    void newFileName(QString);
    void changesNotSaved();
    void saved();



};

#endif // SCRIPTEDITOR_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include <QtSerialPort/QSerialPort>
#include <QtWidgets>
#include "../scripteditor.h"



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool        openScriptFile(const QString &filename);
    QMenu*      menuFile;
    //QMenu *menuScripts;
    QAction*    actionNewScript;
    QAction*    actionOpenScript;
    QAction*    actionSaveScript;
    QAction*    actionSaveScriptAs;
    QAction*    actionSaveAll;
    QAction*    actionCloseScript;

    QMenu*      menuEdit;
    QAction*    actionUndo;
    QAction*    actionRedo;
    QAction*    actionCut;
    QAction*    actionCopy;
    QAction*    actionCopyAsHTML;
    QAction*    actionPaste;
    QAction*    actionComment;
    QAction*    actionUncomment;

    void retranslateUi() {


        setWindowTitle(QApplication::translate("Main", "StateScript Editor", 0));


        menuFile->setTitle(QApplication::translate("Main", "File", 0));
        //menuScripts->setTitle(QApplication::translate("Main", "Scripts", 0));

        actionNewScript->setText(QApplication::translate("Main", "New", 0));
        actionOpenScript->setText(QApplication::translate("Main", "Open...", 0));
        actionSaveScript->setText(QApplication::translate("Main", "Save", 0));
        actionSaveScriptAs->setText(QApplication::translate("Main", "Save as...", 0));
        actionSaveAll->setText(QApplication::translate("Main", "Save all", 0));
        actionCloseScript->setText(QApplication::translate("Main", "Close", 0));

        menuEdit->setTitle(QApplication::translate("Main", "Edit", 0));
        actionUndo->setText(QApplication::translate("Main", "Undo", 0));
        actionRedo->setText(QApplication::translate("Main", "Redo", 0));
        actionCut->setText(QApplication::translate("Main", "Cut", 0));
        actionCopy->setText(QApplication::translate("Main", "Copy", 0));
        actionCopyAsHTML->setText(QApplication::translate("Main", "Copy as HTML", 0));
        actionPaste->setText(QApplication::translate("Main", "Paste", 0));
        actionComment->setText(QApplication::translate("Main", "Comment", 0));
        actionUncomment->setText(QApplication::translate("Main", "Uncomment", 0));

    }

private:

     QTabWidget*            scriptTabs;
     QGridLayout*           mainLayout;
     QVector<bool>          textHighlighed;
     QVector<bool>          isUndoAvailable;
     QVector<bool>          isRedoAvailable;

     //scriptEditor*          editorWindow;
     //QString                currentScriptFile;

     //bool                   unSavedData;
     //bool                   scriptFileOpen;
     //bool                   fileHasName;



protected:
    void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *);
    void moveEvent(QMoveEvent *);
    //void paintEvent(QPaintEvent *);

private slots:
    void tabChanged(int index);
    void setHighlighted(bool highlighted);
    void commentSelectedText();
    void uncommentSelectedText();
    void undoAvailable(bool yes);
    void redoAvailable(bool yes);
    void undoSelected();
    void redoSelected();

public slots:
     void createNewScript();
     void openScriptFile();
     void saveScript();
     void saveScriptAs();
     void saveScript(int);
     void saveScriptAs(int);
     void saveAll();
     void closeScript(int tabIndex);
     void closeScript();
     void copyAsHTML();


     void updateTabNames();
     void updateFileMenu();

signals:


};


#endif // MAINWINDOW_H

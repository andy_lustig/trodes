include(../../module_defaults.pri)

QT       += core gui widgets

TARGET = stateScriptEditor
TEMPLATE = app

#INCLUDEPATH  += ../../Trodes/src-main
#INCLUDEPATH  += ../../Trodes/src-config

SOURCES += main.cpp\
        mainWindow.cpp\
        ../scripteditor.cpp \
        ../highlighter.cpp \
    customApplication.cpp \
   # ../../Trodes/src-main/eventHandler.cpp \
   # ../../Trodes/src-main/trodesdatastructures.cpp

HEADERS  += mainWindow.h\
            ../scripteditor.h \
            ../highlighter.h \
    customApplication.h \
   # ../../Trodes/src-main/eventHandler.h \
   # ../../Trodes/src-main/trodesdatastructures.h

macx {
ICON        = trodesMacIcon_sseditor.icns
OTHER_FILES += \
    Info.plist
QMAKE_INFO_PLIST += Info.plist
}

win32 {
RC_ICONS += trodesWindowsIcon_sseditor.ico
}

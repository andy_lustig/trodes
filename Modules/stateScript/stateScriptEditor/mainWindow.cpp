#include "mainWindow.h"
#include <QDebug>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent) {

    if (objectName().isEmpty())
        setObjectName(QString("Main"));

    resize(800, 600);
    //unSavedData = false;
    //scriptFileOpen = false;
    //fileHasName = false;


    //menu setup
    //-------------------------------------------
    menuFile = new QMenu;
    menuBar()->addAction(menuFile->menuAction());

    //menuScripts = new QMenu;
    //menuFile->addAction(menuScripts->menuAction());
    actionNewScript = new QAction(this);
    menuFile ->addAction(actionNewScript);
    actionOpenScript = new QAction(this);
    menuFile ->addAction(actionOpenScript);
    menuFile ->addSeparator();
    actionSaveScript = new QAction(this);
    menuFile ->addAction(actionSaveScript);
    actionSaveScript->setEnabled(false);
    actionSaveScriptAs = new QAction(this);
    menuFile ->addAction(actionSaveScriptAs);
    actionSaveScriptAs->setEnabled(false);
    actionSaveAll = new QAction(this);
    menuFile->addAction(actionSaveAll);
    actionSaveAll->setEnabled(false);
    actionCloseScript = new QAction(this);
    menuFile ->addAction(actionCloseScript);
    actionCloseScript->setEnabled(false);

    connect(actionNewScript, SIGNAL(triggered()), this, SLOT(createNewScript()));
    connect(actionOpenScript, SIGNAL(triggered()), this, SLOT(openScriptFile()));
    connect(actionSaveScript, SIGNAL(triggered()), this, SLOT(saveScript()));
    connect(actionSaveScriptAs, SIGNAL(triggered()), this, SLOT(saveScriptAs()));
    connect(actionSaveAll, SIGNAL(triggered()), this, SLOT(saveAll()));
    connect(actionCloseScript, SIGNAL(triggered()), this, SLOT(closeScript()));



    menuEdit = new QMenu;
    menuBar()->addAction(menuEdit->menuAction());
    actionUndo = new QAction(this);
    menuEdit->addAction(actionUndo);
    actionRedo = new QAction(this);
    menuEdit->addAction(actionRedo);
    menuEdit->addSeparator();
    actionCut = new QAction(this);
    menuEdit->addAction(actionCut);
    actionCopy = new QAction(this);
    menuEdit->addAction(actionCopy);
    actionCopyAsHTML = new QAction(this);
    menuEdit->addAction(actionCopyAsHTML);
    actionPaste = new QAction(this);
    menuEdit->addAction(actionPaste);
    menuEdit->addSeparator();
    actionComment = new QAction(this);
    menuEdit->addAction(actionComment);
    actionUncomment = new QAction(this);
    menuEdit->addAction(actionUncomment);


    actionUndo->setEnabled(false);
    actionRedo->setEnabled(false);
    actionCut->setEnabled(false);
    actionCopy->setEnabled(false);
    actionCopyAsHTML->setEnabled(false);
    actionPaste->setEnabled(false);
    actionComment->setEnabled(false);
    actionUncomment->setEnabled(false);

    connect(actionUndo,SIGNAL(triggered()),this,SLOT(undoSelected()));
    connect(actionRedo,SIGNAL(triggered()),this,SLOT(redoSelected()));
    connect(actionComment,SIGNAL(triggered()),this,SLOT(commentSelectedText()));
    connect(actionUncomment,SIGNAL(triggered()),this,SLOT(uncommentSelectedText()));
    connect(actionCopyAsHTML,SIGNAL(triggered()),this,SLOT(copyAsHTML()));

    //-------------------------------------------


    //Layouts and tabs-----------------------------------------
    mainLayout =  new QGridLayout();

    //mainLayout->setMargin(0);
    mainLayout->setContentsMargins(QMargins(5,10,5,10));
    //mainLayout->setVerticalSpacing(0);

    scriptTabs = new QTabWidget(this);
    scriptTabs->setStyleSheet("QTabWidget::pane {margin: 0px}") ;
    scriptTabs->setTabPosition(QTabWidget::North);
    scriptTabs->setTabsClosable(true);
    scriptTabs->setMovable(true);
    connect(scriptTabs,SIGNAL(tabCloseRequested(int)),this,SLOT(closeScript(int)));
    connect(scriptTabs,SIGNAL(currentChanged(int)),this,SLOT(tabChanged(int)));

/*
    editorWindow = new scriptEditor(this);
    scriptTabs->addTab(editorWindow, QString("blah blah"));
    connect(editorWindow,SIGNAL(textChanged()),this,SLOT(setUnsavedFile()));
*/
    //mainLayout->addWidget(scriptTabs,1,0);
    //---------------------------------------------------------

    mainLayout->addWidget(scriptTabs,1,0);
    QWidget *window = new QWidget();
    window->setLayout(mainLayout);
    setCentralWidget(window);


    QMetaObject::connectSlotsByName(this);
    retranslateUi();





    //Parse the command line options
    /*
    if (arguments.length() > 1) {
        QString inputFileName = arguments.at(1);
        statusbar->showMessage(inputFileName);
        if (!openScriptFile(inputFileName)) {
           createNewScript();
        }
    } else {
        createNewScript();
    }*/



}

MainWindow::~MainWindow() {

}


void MainWindow::resizeEvent(QResizeEvent *) {

}

void MainWindow::moveEvent(QMoveEvent *) {

}

void MainWindow::updateTabNames() {
    for (int i=0; i<scriptTabs->count();i++) {
        QString tempName;
        scriptEditor* editorPtr = (scriptEditor*)scriptTabs->widget(i);
        tempName = editorPtr->getFileName();
        if (tempName.isEmpty()) {
            tempName = "untitled";
        } else {
            QFileInfo fI = QFileInfo(tempName);
            tempName = fI.fileName();
        }
        if (editorPtr->isUnSaved()) {
            tempName.append("*");
        }
        scriptTabs->setTabText(i,tempName);
    }
}

void MainWindow::createNewScript(){

    scriptEditor* editorWindow = new scriptEditor();
    //editorWindow->setFileName("untitled");
    scriptTabs->addTab(editorWindow, QString(""));
    scriptTabs->setCurrentIndex(scriptTabs->count()-1);

    //connect(editorWindow,SIGNAL(textChanged()),this,SLOT(setUnsavedFile()));

    editorWindow->setData(QByteArray());
    connect(editorWindow,SIGNAL(changesNotSaved()),this,SLOT(updateTabNames()));
    connect(editorWindow,SIGNAL(saved()),this,SLOT(updateTabNames()));
    connect(editorWindow,SIGNAL(copyAvailable(bool)),this,SLOT(setHighlighted(bool)));
    connect(editorWindow,SIGNAL(undoAvailable(bool)),this,SLOT(undoAvailable(bool)));
    connect(editorWindow,SIGNAL(redoAvailable(bool)),this,SLOT(redoAvailable(bool)));
    editorWindow->trackIfSaved();

    updateTabNames();
    updateFileMenu();




    //currentScriptFile = QString("untitled");
    //setWindowTitle(QString("stateScript ") + currentScriptFile);
    //scriptFileOpen = true;
    //fileHasName = false;

    //actionSaveScript->setEnabled(true);
    //actionSaveScriptAs->setEnabled(true);
    //actionCloseScript->setEnabled(true);

    //actionNewScript->setEnabled(false);
    //actionOpenScript->setEnabled(false);

    //actionUndo->setEnabled(false);
    //actionRedo->setEnabled(false);
    //actionCut->setEnabled(true);
    //actionCopy->setEnabled(true);
    //actionPaste->setEnabled(false);

}

void MainWindow::openScriptFile() {
    //With no inputs, this overloaded function first
    //opens a dialog to select a file, then passes the
    //filename to the other version of the function.

    QString fileName = QFileDialog::getOpenFileName(0, QString("Open stateScript file"),0,QString("*.sc"));
    if (!fileName.isEmpty()) {
        openScriptFile(fileName);
    }
}

bool MainWindow::openScriptFile(const QString &fileName){

    bool alreadyOpen = false;

    //check to see if the file is already open in one of the tabs
    for (int i=0; i<scriptTabs->count();i++) {
        scriptEditor* editorPtr = (scriptEditor*)scriptTabs->widget(i);
        QString tempName = editorPtr->getFileName();
        if (tempName.compare(fileName) == 0) {
            alreadyOpen = true;
            scriptTabs->setCurrentIndex(i);
            break;
        }
    }

    if (alreadyOpen) {
        return false;
    }
    qDebug() << "Opening file" << fileName << "...";
    QFile file(fileName);
    if (file.open(QFile::ReadOnly | QFile::Text)) {

        scriptEditor* editorWindow = new scriptEditor();
        editorWindow->setFileName(fileName);
        scriptTabs->addTab(editorWindow,"");
        textHighlighed.push_back(false);
        isUndoAvailable.push_back(false);
        isRedoAvailable.push_back(false);
        editorWindow->setData(file.readAll());
        connect(editorWindow,SIGNAL(changesNotSaved()),this,SLOT(updateTabNames()));
        connect(editorWindow,SIGNAL(saved()),this,SLOT(updateTabNames()));
        connect(editorWindow,SIGNAL(copyAvailable(bool)),this,SLOT(setHighlighted(bool)));
        connect(editorWindow,SIGNAL(undoAvailable(bool)),this,SLOT(undoAvailable(bool)));
        connect(editorWindow,SIGNAL(redoAvailable(bool)),this,SLOT(redoAvailable(bool)));
        editorWindow->trackIfSaved();
        //currentScriptFile = fileName;
        //setWindowTitle(QString("stateScript ") + currentScriptFile);
        file.close();
        scriptTabs->setCurrentIndex(scriptTabs->count()-1);
        updateTabNames();
        updateFileMenu();

        //scriptFileOpen = true;
        //fileHasName = true;
        //unSavedData = false;

        /*
        actionSaveScript->setEnabled(true);
        actionSaveScriptAs->setEnabled(true);
        actionCloseScript->setEnabled(true);
        actionNewScript->setEnabled(false);
        actionOpenScript->setEnabled(false);

        actionUndo->setEnabled(false);
        actionRedo->setEnabled(false);
        actionCut->setEnabled(true);
        actionCopy->setEnabled(true);
        actionPaste->setEnabled(false);*/

        return true;


    } else {
        qDebug() << "File could not be opened";
        return false;
    }

}

void MainWindow::saveAll() {
    for (int i=0;i<scriptTabs->count();i++) {
        saveScript(i);
    }
}

void MainWindow::saveScript() {
    int currentTab = scriptTabs->currentIndex();
    saveScript(currentTab);
}

void MainWindow::saveScriptAs() {
    int currentTab = scriptTabs->currentIndex();
    saveScriptAs(currentTab);
}

void MainWindow::saveScript(int currentTab) {


    scriptEditor* editorWindow = (scriptEditor*) scriptTabs->widget(currentTab);
    if (!editorWindow->getFileName().isEmpty()) {
        QFile file(editorWindow->getFileName());
        if ( file.open(QIODevice::WriteOnly) ) {
            QTextStream TextStream(&file);
            QString docString = editorWindow->document()->toPlainText();


            TextStream << docString;
            file.close();
            editorWindow->setSavedFile();

            //setWindowTitle(QString("stateScript ") + currentScriptFile);
            //unSavedData = false;


            //actionNewScript->setEnabled(false);
            //actionOpenScript->setEnabled(false);

        }
    } else {
        saveScriptAs();
    }

}

void MainWindow::saveScriptAs(int currentTab) {


    scriptEditor* editorWindow = (scriptEditor*) scriptTabs->widget(currentTab);

    QString tempName = editorWindow->getFileName();
    if (tempName.isEmpty()) {
        tempName = "untitled";
    }

    QFileInfo fI = QFileInfo(tempName);
    QString filePath = fI.absolutePath();
    QString defaultName = fI.baseName();


    QStringList filenames;
    QString filename;
    QFileDialog dialog(0,"Save file as");


    dialog.setDirectory(filePath);
    dialog.setDefaultSuffix("sc");
    dialog.selectFile(defaultName+"_copy");
    //dialog.selectFile(currentScriptFile);

    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setOption(QFileDialog::DontConfirmOverwrite,false);
    //dialog.setOption(QFileDialog::DontUseNativeDialog);
    if (dialog.exec()) {
        filenames = dialog.selectedFiles();
    }
    if(filenames.size() == 1) {
        filename = filenames.first();
    }
    if (!filename.isEmpty()) {

        QFile file(filename);


        if ( file.open(QIODevice::WriteOnly) ) {
            QTextStream TextStream(&file);
            QString docString = editorWindow->document()->toPlainText();


            TextStream << docString;
            file.close();

            editorWindow->setFileName(filename);
            editorWindow->setSavedFile();

            //currentScriptFile = filename;
            //setWindowTitle(QString("stateScript ") + currentScriptFile);
            //unSavedData = false;
            //fileHasName = true;
            //actionNewScript->setEnabled(false);
            //actionOpenScript->setEnabled(false);

        }
    }


}

void MainWindow::copyAsHTML() {
    int currentTab = scriptTabs->currentIndex();
    scriptEditor* editorWindow = (scriptEditor*) scriptTabs->widget(currentTab);
    QString htmlString;
    //editorWindow->scriptHighlighter->asHtml(htmlString);
    editorWindow->asHtml(htmlString);
    //htmlString.replace("\n","");
    //htmlString.replace("\t","");
    //htmlString.replace("\\","");
    QClipboard *clipboard = QApplication::clipboard();
    //QString originalText = clipboard->text();

    clipboard->setText(htmlString);

    //qDebug() << htmlString.toLocal8Bit();
}

void MainWindow::closeScript() {
    closeScript(scriptTabs->currentIndex());
}

void MainWindow::closeScript(int tabIndex) {

    scriptEditor* editorWindow = (scriptEditor*) scriptTabs->widget(tabIndex);

    QMessageBox::StandardButton reply;

    if (editorWindow->isUnSaved()) {
        //Warn that there is unsaved data
        QMessageBox messageBox;
        reply = messageBox.question(0,"Unsaved data","You have unsaved changes. Close anyway?",QMessageBox::Cancel|QMessageBox::Close);

    } else {
        reply = QMessageBox::Close;
    }

    if (reply == QMessageBox::Close) {
        //editorWindow->removeData();
        //currentScriptFile = "";
        //setWindowTitle(QString("stateScript "));
        //unSavedData = false;
        //scriptFileOpen = false;
        //fileHasName = false;

        scriptTabs->removeTab(tabIndex);
        updateFileMenu();

        textHighlighed.removeAt(tabIndex);
        isUndoAvailable.removeAt(tabIndex);
        isRedoAvailable.removeAt(tabIndex);

        int currentTab = scriptTabs->currentIndex();
        tabChanged(currentTab);

    }


}

void MainWindow::commentSelectedText() {
    //used to add '%' comment markers to the beginning of
    //each line in the selection

    int currentTab = scriptTabs->currentIndex();
    scriptEditor* editorWindow = (scriptEditor*) scriptTabs->widget(currentTab);
    int startPos = editorWindow->textCursor().selectionStart();
    int endPos = editorWindow->textCursor().selectionEnd();

    QString currentDoc = editorWindow->document()->toPlainText();

    bool addComment = true;
    editorWindow->textCursor().beginEditBlock();
    for (int i = startPos; i <= endPos; i++) {
        QChar currentChar = currentDoc.at(i);
        if ((int)currentChar.toLatin1() == 10) {
            addComment = true;
        } else if (addComment) {
            currentDoc.insert(i,QString("%"));

            endPos++;
            i++;
            addComment = false;
        }

    }

    if (currentDoc.length()-endPos-1 > 0) {
        currentDoc.remove(endPos,currentDoc.length()-endPos);
    }
    currentDoc.remove(0,startPos);

    editorWindow->textCursor().removeSelectedText();
    editorWindow->textCursor().insertText(currentDoc);

    editorWindow->textCursor().endEditBlock();
}

void MainWindow::uncommentSelectedText() {
    //used to remove '%' comment markers at the beginning of
    //each line in the selection
    int currentTab = scriptTabs->currentIndex();
    scriptEditor* editorWindow = (scriptEditor*) scriptTabs->widget(currentTab);
    int startPos = editorWindow->textCursor().selectionStart();
    int endPos = editorWindow->textCursor().selectionEnd();

    QString currentDoc = editorWindow->document()->toPlainText();
    bool foundEndOfLine = true;
    editorWindow->textCursor().beginEditBlock();
    for (int i = startPos; i <= endPos; i++) {
        QChar currentChar = currentDoc.at(i);
        if ((int)currentChar.toLatin1() == 10) {
            foundEndOfLine = true;
        } else if (foundEndOfLine) {
            if (currentChar == '%') {
                currentDoc.remove(i,1);
                endPos--;
            }
            foundEndOfLine = false;
        }

    }

    if (currentDoc.length()-endPos-1 > 0) {
        currentDoc.remove(endPos,currentDoc.length()-endPos);
    }
    currentDoc.remove(0,startPos);

    editorWindow->textCursor().removeSelectedText();
    editorWindow->textCursor().insertText(currentDoc);

    editorWindow->textCursor().endEditBlock();
}

void MainWindow::undoSelected() {
    int currentTab = scriptTabs->currentIndex();
    scriptEditor* editorWindow = (scriptEditor*) scriptTabs->widget(currentTab);
    editorWindow->undo();
}

void MainWindow::redoSelected() {
    int currentTab = scriptTabs->currentIndex();
    scriptEditor* editorWindow = (scriptEditor*) scriptTabs->widget(currentTab);
    editorWindow->redo();
}

void MainWindow::tabChanged(int index) {
    if (textHighlighed[index]) {
        actionComment->setEnabled(true);
        actionUncomment->setEnabled(true);
    } else {
        actionComment->setEnabled(false);
        actionUncomment->setEnabled(false);
    }

    actionUndo->setEnabled(isUndoAvailable[index]);
    actionRedo->setEnabled(isRedoAvailable[index]);

}

void MainWindow::setHighlighted(bool highlighted) {

    int currentTab = scriptTabs->currentIndex();
    textHighlighed[currentTab] = highlighted;
    if (highlighted) {
        actionComment->setEnabled(true);
        actionUncomment->setEnabled(true);
    } else {
        actionComment->setEnabled(false);
        actionUncomment->setEnabled(false);
    }
}

void MainWindow::undoAvailable(bool yes) {
    int currentTab = scriptTabs->currentIndex();
    isUndoAvailable[currentTab] = yes;

    actionUndo->setEnabled(yes);

}

void MainWindow::redoAvailable(bool yes) {
    int currentTab = scriptTabs->currentIndex();
    isRedoAvailable[currentTab] = yes;
    actionRedo->setEnabled(yes);

}

void MainWindow::updateFileMenu() {
    int currentTab = scriptTabs->currentIndex();
    if (currentTab > -1) {
        actionSaveScript->setEnabled(true);
        actionSaveScriptAs->setEnabled(true);
        actionCloseScript->setEnabled(true);

        actionUndo->setEnabled(false);
        actionRedo->setEnabled(false);
        actionCut->setEnabled(true);
        actionCopy->setEnabled(true);
        actionCopyAsHTML->setEnabled(true);
        actionPaste->setEnabled(false);
        //scriptEditor* editorWindow = (scriptEditor*) scriptTabs->widget(currentTab);
    } else {
        actionSaveScript->setEnabled(false);
        actionSaveScriptAs->setEnabled(false);
        actionCloseScript->setEnabled(false);
        actionNewScript->setEnabled(true);
        actionOpenScript->setEnabled(true);

        actionUndo->setEnabled(false);
        actionRedo->setEnabled(false);
        actionCut->setEnabled(false);
        actionCopy->setEnabled(false);
        actionCopyAsHTML->setEnabled(false);
        actionPaste->setEnabled(false);
    }

}

void MainWindow::closeEvent(QCloseEvent* event) {
    QMessageBox::StandardButton reply;

    bool unSavedData = false;
    for (int i=0; i < scriptTabs->count(); i++) {
        scriptEditor* editorWindow = (scriptEditor*) scriptTabs->widget(i);
        if (editorWindow->isUnSaved()) {
            unSavedData = true;
        }

    }

    if (unSavedData) {
        //Warn that there is unsaved data
        QMessageBox messageBox;
        reply = messageBox.question(0,"Unsaved data","You have unsaved changes. Discard?",QMessageBox::Cancel|QMessageBox::Discard);

    } else {
        reply = QMessageBox::Discard;
    }

    if (reply == QMessageBox::Discard) {
        event->accept();
    } else {
        event->ignore();
    }

}


//--------------------------------------------------------------------------



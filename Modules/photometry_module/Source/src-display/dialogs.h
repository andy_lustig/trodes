/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SOUNDDIALOG_H
#define SOUNDDIALOG_H

#include <QDialog>
#include <QSlider>
#include <QLabel>
#include <QtWidgets>

#define PHOTOMETRYMODULE_STANDALONEMODE 1
#define PHOTOMETRYMODULE_SLAVEMODE 2
#define PHOTOMETRYMODULE_CAMERASTREAMINGMODE 8
#define PHOTOMETRYMODULE_FILEPLAYBACKMODE 16

//-----------------------------------------

class TrodesButton : public QPushButton {

Q_OBJECT

public:
    TrodesButton(QWidget *parent = 0);
    void setRedDown(bool yes);

};
//--------------------------------------

class waveformGeneratorDialog : public QWidget
{

Q_OBJECT

public:
    waveformGeneratorDialog(double currentCarrierFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent = 0);
 //   waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent = 0);

    QDoubleSpinBox *carrierFreqSpinBox;
    QSlider *freqSlider;
    QSlider *ampSlider;
    QSlider *threshSlider;
    QLabel *freqDisplay;
    QLabel *ampDisplay;
    QLabel *threshDisplay;
    QLabel *carrierFreqTitle;
    QLabel *freqTitle;
    QLabel *ampTitle;
    QLabel *threshTitle;

protected:
    void closeEvent(QCloseEvent* event);

private:

signals:
    void windowClosed(void);
};

#endif // SOUNDDIALOG_H

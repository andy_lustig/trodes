/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STREAMDISPLAY_H
#define STREAMDISPLAY_H

#include <QtGui>
#include "configuration.h"
#include "iirFilter.h"
#include "streamProcessorThread.h"
#include "../../../Source/src-main/sharedVariables.h"
#include <QtWidgets>

class StreamWidgetGL : public QGLWidget {

  Q_OBJECT

public:
  StreamWidgetGL(QWidget *parent, QList<int> nTrodes, StreamProcessorManager* managerPtr);
  ~StreamWidgetGL();

public:


private:

  StreamProcessorManager* streamManager;
  QList<int> dispHWChannels;
  int nChan; // number of channels to display
  int currentHighlightChannel;
  QColor backgroundColor;

  double TLength;
  double FS;

  QImage bkg;

  QVector<int> maxAmplitude;
  QVector<QColor> traceColor;


  int PSTHTriggerHeaderChannel;
  bool PSTHTriggerState;

  int settleControlChannel; //the header channel that controls auto settle command
  quint8 settleControlChannelTriggerState; //0 = off, 1 = downward edge, 2 = upward edge

  QOpenGLShaderProgram *m_program;
  int m_projMatrixLoc;
  int m_mvMatrixLoc;
  int m_colorLoc;
  QMatrix4x4 m_proj;
  QMatrix4x4 m_modelView;
  QOpenGLVertexArrayObject m_vao;
  QOpenGLBuffer *m_vbo;
  GLuint m_linesBuf;
  GLuint m_rectVertBuf;
  GLuint m_rectElemBuf;


  QOpenGLDebugLogger *logger;

protected:
  void setupViewport(int width, int height);
  void resizeGL(int w, int h);
  void initializeGL();
  void paintEvent(QPaintEvent *event);
  void mousePressEvent(QMouseEvent *);
  void wheelEvent(QWheelEvent *);

signals:

  void channelClicked(int hwchannel);
  void settingsMenuRequested(int hwchannel);

public slots:

  void setHighlightChannel(int hardwareChannel);
  void updateAxes();
  void setTLength(double T);
  void setChannels(void);
  void updateMaxDisplay(void);
  void updateTraceColor(void);
  void updateBackgroundColor();
  void stopAcquisition(void);

private slots:
   void showChannelContextMenu(const QPoint& pos, int channel);
   void showNtrodeColorSelector(int channel);
   void nTrodeColorChosen(QColor c);
   void nTrodeColorSelectorCanceled();

   void showBackgroundColorSelector();
   void backgroundColorChosen(QColor c);
   void backgroundColorSelectorCanceled();


};

class StreamDisplayManager : public QWidget {

  Q_OBJECT

public:
  StreamDisplayManager(QWidget *parent, StreamProcessorManager* managerPtr);
  ~StreamDisplayManager();

  QList<QWidget*>           eegDisplayWidgets;
  QList<QList<int> >        streamDisplayChannels;
  QList<QList<int> >        nTrodeIDs;
  QList<StreamWidgetGL *>   glStreamWidgets;
  QList<int>                columnsPerPage;

private:

  QList<QGridLayout*>       eegDisplayLayout;
  StreamProcessorManager*   streamManager;
  bool                      displayFrozen;
  QTimer*                   updateTimer;

private slots:
  void updateAllColumns();


public slots:
  void relayChannelClick(int hwChannel);
  void updateAudioHighlightChannel(int hwChan);
  void freezeDisplay(bool);

signals:
  void streamChannelClicked(int channel);
  void trodeSelected(int nTrode);

};

#endif // STREAMDISPLAY_H

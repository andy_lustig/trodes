/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NIUSBDAQTHREAD
#define NIUSBDAQTHREAD


#include <QThread>
#include <QVector>
#include <QtNetwork>
#include "abstractTrodesSource.h"
#include "qmath.h"

#ifdef WIN32
    #include <windows.h>
    #include "C:\Program Files (x86)\National Instruments\NI-DAQ\DAQmx ANSI C Dev\include\NIDAQmx.h"
#elif __MACH__
    #include "/Applications/National Instruments/NI-DAQmx Base/includes/NIDAQmxBase.h"
#endif


class NIUSBDAQRuntime : public AbstractSourceRuntime {
  Q_OBJECT
public:
  NIUSBDAQRuntime(QObject *parent);
  ~NIUSBDAQRuntime(void);

private:

#ifdef WIN32
  TaskHandle taskHandle;
#endif

public slots:
  void  Run(void);

};

class NIUSBDAQInterface : public AbstractTrodesSource {
  Q_OBJECT

public:

    NIUSBDAQInterface(QObject *parent);
    ~NIUSBDAQInterface(void);

    int state;


private:

  NIUSBDAQRuntime *NIUSBDataProcessor;

private slots:

signals:

public slots:
    void InitInterface(void);
    void StartAcquisition(void);
    void StopAcquisition(void);
    void CloseInterface(void);

    void startRecord(void);
    void stopRecord(void);

};


#endif // NIUSBDAQTHREAD

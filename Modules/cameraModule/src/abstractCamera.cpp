#include "abstractCamera.h"

AbstractCamera::AbstractCamera() {
    fmt = Fmt_Invalid;
}

void AbstractCamera::sendFrameSignals(QImage *img, uint32_t frameCount, bool flip) {
    if (frameCount == 0) {
        numFramesRecieved++;
    } else {
        numFramesRecieved = frameCount;
    }
    emit newFrame(img,numFramesRecieved, flip);
    emit newFrame();

}

void AbstractCamera::setFormat(AbstractCamera::videoFmt format) {
    fmt = format;
    emit formatSet(fmt);
}

AbstractCamera::videoFmt AbstractCamera::getFormat() {
    return fmt;
}

void AbstractCamera::blinkAcquire() {

}

void AbstractCamera::resetCurrentCamera() {

}


from pyCameraModule import AbstractPosHandler
import math
import time
import datetime as dt


class MyPosHandler(AbstractPosHandler):

    def __init__(self,pos):
        AbstractPosHandler.__init__(self)
        self.lastPos = None
        self.lastTime = None
        self.averageFramePeriod = 1/30
        self.pos = pos
        

    def newDataPoint(self,newPoint):
        global position
        
        if not self.lastPos is None:
            
            #keep a moving average of the frame rate
            #timeDiff = time.clock()-self.lastTime
            #timeDiff = dt.datetime.now().second-self.lastTime
            #self.lastTime = time.clock()
            
            #print dt.datetime.now()
            #self.averageFramePeriod = (self.averageFramePeriod - (self.averageFramePeriod / 30)) + (timeDiff/30);
            #calculate the distance moved in pixels
            speed = math.sqrt(math.pow((newPoint.x() - self.lastPos.x()),2)+math.pow((newPoint.y() - self.lastPos.y()),2))
            #position.setSpeed(speed) #runs function on c++ side
            self.pos.setSpeed(speed)
            #print 1/self.averageFramePeriod
        
        #self.lastTime = time.clock()
        #self.lastTime = dt.datetime.now()
            
        self.lastPos = newPoint;
 
            
        
        
        

include(../module_defaults.pri)

QT       += opengl core gui network xml multimedia widgets

TARGET = cameraModule
TEMPLATE = app

# Use Qt Resource System for images
RESOURCES += \
    $$TRODES_REPO_DIR/Resources/Images/buttons.qrc

INCLUDEPATH  += ../../Trodes/src-main
INCLUDEPATH  += ../../Trodes/src-config
INCLUDEPATH  += ./src

#unix: QMAKE_CXXFLAGS += -D__STDC_CONSTANT_MACROS
QMAKE_CFLAGS += -g -O3
# Requied for some C99 defines
DEFINES += __STDC_CONSTANT_MACROS

#-------------------------------------------------
# Set up embedded python
CONFIG += EMBEDPYTHON

EMBEDPYTHON {
DEFINES += PYTHONEMBEDDED

SOURCES += src/pythoncamerainterface.cpp
HEADERS += src/pythoncamerainterface.h

# Set up the python libs & headers the same way as they were set up for PythonQt build
# (location/version of Python used on each platform is set there)
include ( $$TRODES_REPO_DIR/PythonQt3.0/build/python.prf )

# Set up PythonQt libs & headers (NB don't include 'common.prf'--it's
# used for building PythonQt itself and will munge your target names)

include ( $$TRODES_REPO_DIR/PythonQt3.0/build/PythonQt.prf )
include ( $$TRODES_REPO_DIR/PythonQt3.0/build/PythonQt_QtAll.prf )

}
#-------------------------------------------------


#-------------------------------------------------
# Set up shared libraries for Mac build/deployment.
#
# NB On the Mac, each shared library (dylib) has an 'install name" which should
# be a path at which the dylib can be found by a calling executable. The name
# is copied to the executable at link time, and used to find the library at run
# time. If that link is incomplete (e.g. 'libPythonQt.dylib', instead of a full
# path), then the system loader will fall back to searching system library
# paths (~/lib, /System/Frameworks, etc), and so cameraModule will often still
# run.
#
# Unfortunately 'macdeployqt' (a tool provided by Qt to copy external libraries
# into an app bundle and rewrite all the shared library paths) is not as smart
# as the system loader. It will only succeed if all the library links in the
# executable are full paths to those libraries.
#
# We therefore want to ensure that any external shared libraries have 'install
# names' set to absolute paths *before* linking cameraModule. This has the
# result that 1) camMod 'just works', and 2) we get to use macdeployqt and save
# ourselves a lot of deployment hassle
#
# Here's the overall strategy:
#
# 1) Use install names beginning with @rpath *only* for the Qt libraries (this
# is handled well by macdeployqt). Setting the @rpath correctly for the
# executable is handled by build_defaults.pri, since it applies to all
# projects.
#
# 2) For all other dependencies, make sure we are linking against libraries
# that have correct, absolute paths as their 'install name'
#
#  -On the Mac, if you have FFMPEG libs (libavcodec, libswscale) and libx264
#   provided by homebrew (in /usr/local/lib), they should already be correctly
#   installed.
#
#  -libPythonQt libs do *not* have their install name set correctly at build
#   time, so we run a script (fix_pythonqt_dylib.sh) to fix this before #
#   compiling. (In addition, libPythonQt_QtAll.dylib links to
#   libPythonQt.dylib. # In order to have this link survive the deploy process,
#   we update this link # to use @loader_path.)
#
#  -libPvAPI (provided in the Trodes repository) also needs to have its
#   install name set to an absolute path. We do this with a single call to #
#   install_name_tool
#
# 3) [**Optional, not done as part of the build process**] We can now run
# 'macdeployqt' against the resulting bundles. It will successfully copy over all
# the needed Qt libs, Qt plugins, *and* external libraries to the app bundle's
# 'Contents/Frameworks' folder, and update all the links to be relative to the
# @executable_path. This bundle can then be copied and run on a machine without
# Qt installed.
#
# Gotchas:
# There is a bug in macdeployqt for 5.5 (fixed in 5.7) that won't correctly
# deploy libs if they're in a folder called 'lib'. So we rely on a symlink
# called dylibs/ instead, which we use for the install_names.
#
# See this link for a good discussion of install_names, dylib loading, etc:
# https://www.mikeash.com/pyblog/friday-qa-2009-11-06-linking-and-install-names.html

macx{

EMBEDPYTHON{
# Run the fix_pythonqt_dylibs.sh shell script: 
PQT_DYLIB_DIR=$${TRODES_REPO_DIR}/PythonQt3.0/dylibs # NB No trailing slash--macdeployqt's path handling is fragile
fix_dylibs.commands += "$${PQT_DYLIB_DIR}/fix_pythonqt_dylib.sh $${PQT_DYLIB_DIR} $$DEBUG_EXT ;" # ($$DEBUG_EXT is set in PythonQt.prf)
}

# Set the install_name of PvAPI.dylib to its absolute location so that it will
# be linked correctly
PVAPI_DYLIB=$${PWD}/libraries/osx/GigESDK/bin-pc/x64/libPvAPI.dylib
fix_dylibs.commands += "install_name_tool -id $$PVAPI_DYLIB $$PVAPI_DYLIB && echo \"==> PvAPI.dylib install_name updated before build:\" ;"
fix_dylibs.commands += "otool -L $$PVAPI_DYLIB | head -2;"

# Set up the Makefile to run these commands before compiling
first.depends = $(first) fix_dylibs
export(first.depends)
export(fix_dylibs.commands)
QMAKE_EXTRA_TARGETS += first fix_dylibs
}

#-------------------------------------------------
# Icon stuff for Mac
macx {
ICON        = ./src/trodesMacIcon_camera.icns
OTHER_FILES += ./src/Info.plist
QMAKE_INFO_PLIST += ./src/Info.plist
}

#-------------------------------------------------
# FFMPEG libraries
unix:!macx: {

# Option 1: Build against system FFMPEG libs, or those in /usr/local
#INCLUDEPATH += /usr/local/include /usr/include
#LIBS += -L"/usr/local/lib" -L"/usr/lib" -lavcodec -lavformat -lswscale -lavutil -lx264 -lavresample

# Option 2: Build against FFMPEG libraries in Trodes source tree
INCLUDEPATH += ../cameraModule/libraries/linux/x264
LIBS += -L"../cameraModule/libraries/linux/x264" -lavcodec -lavformat -lswscale -lavutil -lx264 -lavresample
}

unix:!macx{

# For Linux, use the Qt-provided file installation directive ('INSTALLS') to
# specify shared libraries should be copied over to the bin/linux/Libraries
# directory upon running 'make install'
#
# (NB in future, this approach could also be used on Windows)

libraries.path = $$TRODES_LIB_INSTALL_DIR
libraries.files += \
    # use 'ldd cameraModule' to find which libraries (and version) to include
    ./libraries/linux/x264/libavcodec.so.56\
    ./libraries/linux/x264/libavformat.so.56\
    ./libraries/linux/x264/libswscale.so.3\
    ./libraries/linux/x264/libavutil.so.54\
    ./libraries/linux/x264/libx264.so.148\
    ./libraries/linux/x264/libavresample.so.2\ # (needed by other FFMPEG libs)
    ./libraries/linux/GigESDK/bin-pc/x64/libPvAPI.so\
    $$TRODES_REPO_DIR/PythonQt3.0/Python/lib/libpython3.5m.so.1.0\
    $$TRODES_REPO_DIR/PythonQt3.0/lib/libPythonQt.so.1\
    $$TRODES_REPO_DIR/PythonQt3.0/lib/libPythonQt_QtAll.so.1
INSTALLS += libraries
}

macx {
# build against FFMPEG installed in /usr/local/{include,lib} (e.g. by homebrew,
# using 'brew install ffmpeg x264')
INCLUDEPATH += /usr/local/include
LIBS += -L"/usr/local/lib" -lavcodec -lavformat -lswscale -lavutil  -lavresample -lx264
}

win32 {
RC_ICONS += ./src/trodesWindowsIcon_camera.ico
INCLUDEPATH += ../cameraModule/libraries/win32
INCLUDEPATH += ../cameraModule/libraries/win32/libx264

# build against FFMPEG libraries in Trodes source tree
FFMPEG_LIBRARY_PATH = ../cameraModule/libraries/win32
LIBS += -L$$FFMPEG_LIBRARY_PATH
LIBS       += -lavutil -lavcodec -lavformat -lswscale -lx264-142
}
#-------------------------------------------------


#-------------------------------------------------
# Settings required for Allied Vision GigE libraries
CONFIG += AVTGIGECAMERA

AVTGIGECAMERA {
DEFINES += AVT_GIGE
SOURCES += src/avtWrapper.cpp
HEADERS += src/avtWrapper.h

macx {
LIBS += -L"$$PWD/libraries/osx/GigESDK/bin-pc/x64" -lPvAPI
INCLUDEPATH += "$$PWD/libraries/osx/GigESDK/inc-pc"
}

win32 {
LIBS += -L"$$PWD/libraries/win32/GigESDK/lib-pc" -lPvAPI
INCLUDEPATH += "$$PWD/libraries/win32/GigESDK/inc-pc"
}

unix {
DEFINES += _x64
LIBS += -L"$$PWD/libraries/linux/GigESDK/bin-pc/x64" -lPvAPI
INCLUDEPATH += "$$PWD/libraries/linux/GigESDK/inc-pc"
}
}
#-------------------------------------------------


#----------------------------------------------------------
# Settings required for GigE Vision (aravis) interface

#CONFIG += GIGECAMERA

GIGECAMERA {
DEFINES += ARAVIS
QMAKE_CXXFLAGS += -std=gnu++11
SOURCES += src/araviswrapper.cpp
HEADERS += src/araviswrapper.h

unix: LIBS += -L/usr/local/lib/ -laravis-0.4

INCLUDEPATH += /usr/local/include/aravis-0.4
DEPENDPATH += /usr/local/include/aravis-0.4


unix:!macx: LIBS += -L/usr/lib/x86_64-linux-gnu/ -lglib-2.0 -lgobject-2.0

INCLUDEPATH += /usr/include/glib-2.0
DEPENDPATH += /usr/include/glib-2.0

INCLUDEPATH += /usr/lib/x86_64-linux-gnu/glib-2.0/include/

INCLUDEPATH += /usr/include/glib-2.0/gobject/
}
#-----------------------------------------------------------


SOURCES += ../../Trodes/src-display/sharedtrodesstyles.cpp
HEADERS += ../../Trodes/src-display/sharedtrodesstyles.h

SOURCES += src/main.cpp\
        src/mainwindow.cpp \
        src/videoDisplay.cpp \
        ../../Trodes/src-main/trodesSocket.cpp \
        ../../Trodes/src-config/configuration.cpp \
        src/videoDecoder.cpp \
        src/videoEncoder.cpp \
        src/dialogs.cpp \
        src/abstractCamera.cpp \
        src/webcamWrapper.cpp \
        ../../Trodes/src-main/trodesdatastructures.cpp \
        ../../Trodes/src-main/eventHandler.cpp \



HEADERS  += src/mainwindow.h \
        src/videoDisplay.h \
        ../../Trodes/src-main/trodesSocket.h \
        ../../Trodes/src-config/configuration.h \
        src/videoDecoder.h \
        src/videoEncoder.h \
        src/dialogs.h \
        src/abstractCamera.h \
        src/webcamWrapper.h \
        ../../Trodes/src-main/trodesdatastructures.h \
        ../../Trodes/src-main/eventHandler.h \


#RESOURCES += ./src/pyCameraModule.py

DISTFILES += \
    src/pythonPath.txt


+# .pro file debugging messages
#message(Inside $$TARGET: QMAKE_RPATH: $$QMAKE_RPATH)
#message(Inside $$TARGET: QMAKE_RPATHDIR: $$QMAKE_RPATHDIR)




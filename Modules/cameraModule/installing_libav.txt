Instructions for installing the shared libraries needed to allow Trodes'
cameraModule to compile.

PREAMBLE:
The LibAV project (libav.org) was forked from FFMPEG
(ffmpeg.org) in 2011; both projects still provide competing,
identically-named, semi-compatible libraries (libav*). Ubuntu defaults
to libav.org. Per Mattias, trodes on Mac should also be built using a recent
version of LibAV from libav.org. However the version provided in the
libraries/win32 directory seems to be from the FFMPEG project. For now
(as of December 2015), the instructions below are tested on Linux and
Mac.

==========
LINUX (tested on Ubuntu 14.04)
==========
All you need to do is pull down a few packages:

sudo apt-get install \
libavcodec-dev \
libavformat-dev \
libswscale-dev \
libx264-dev \
libbz2-dev

On Ubuntu 14.04, version 54.35.0 is known to work (check version in
/usr/include/libavcodec/version.h)


==========
MAC (tested on OS X 10.11 'El Capitan')
==========


+++ YASM +++

You need the YASM package installed before compiling libav
(dramatically improves performance of compiled libav)

An easy way to get this is using a package manager for mac such as
homebrew (www.brew.sh), or MacPorts (macports.org). If you are using
brew, run: brew install yasm

Alternatively, you can download and build from source, the old-fashioned way:

git clone git://github.com/yasm/yasm.git
cd yasm
./autogen.sh
./configure
make
sudo make install


+++ LibAV +++

Download libav from www.libav.org (latest version 11.4 works as
of December 2015).

(Note you can't use the default homebrew install of libav since it
doesn't compile with shared libraries enabled by default)

NOTE: If you have ever installed ffmpeg, libav, or x264 before, make
sure to completely uninstall any old versions. Look in
/usr/local/include and /usr/local/bin for libraries and folders
beginning in libav*, libswscale*, and x264*. Attempt to uninstall
these cleanly (by running make uninstall from the original build
directory, e.g., or using your package manager of choice, e.g. brew
uninstall). Manually remove old libs if necessary. Tom found that
older version of these libs could also be cached, either by the OS, or
by qt Creator ('qmake clean' didn't resolve this, so I suspect the
OS). Restarting qt Creator, and rebooting the machine are advisable
after uninstalling.

In the downloaded libav source directory:

./configure --disable-static --enable-shared
make
sudo make install


+++ x264 +++

(Ditto above: you can't use the default homebrew install of x264 since
it doesn't compile with shared libraries enabled by default.), so
download and compile from source:

git clone git://git.videolan.org/x264.git
cd x264
./configure --enable-static --enable-shared
make
sudo make install


==========
WINDOWS
==========

+++ LibAV (provided by FFMPEG)+++

"You can just use downloaded versions of the binaries", per
Mattias. The version provided in Modules/cameraModules/ is from the
FFMPEG.org project (not libav as for Linux/Mac above) version 56.8.102
(from July 2014).

+++ x264 +++

(msys)
git clone git://git.videolan.org/x264.git
cd x264
./configure --disable-cli --enable-shared --extra-ldflags=-Wl,--output-def=libx264.def
make

then, generate a visual studio .lib file using visual studio command prompt:
LIB /DEF:libx264.def

You'll need both libx264-XXX.dll and libx264.lib

+++ Python QT (These steps have already been done in repo, but changes to the qt vertsion, compiler version, or python version will require a repeat)+++
1) download Python version 2.7.9 Versions above 2.7.9 (Like 2.7.10, 2.7.11) don't work.  Here, we assume an install directory of c:\Python27 

2) (This step and step 3 may not be required) Add to System PATH
C:\Qt\5.5\mingw492_32\bin;C:\Qt\Tools\mingw492_32\bin

3) From command prompt: 

gendef c:/Windows/system32/python27.dll 
dlltool --input-def python27.def --dllname python27 --output-lib libpython27.a 

then, copy libpython27.a to C:/Python27/libs (replacing the exisiting one)

Change files:

4) python.prf:
win32:PYTHON_VERSION=27
win32:INCLUDEPATH += C:/Python27/include
win32:LIBS += C:/Python27/libs/libpython$${PYTHON_VERSION}$${DEBUG_EXT}.a

5) PythonQt.prf:
win32::LIBS += $$PWD/../lib/libPythonQt$${DEBUG_EXT}.a

6) PythonQt_QtAll.prf:
win32::LIBS += $$PWD/../lib/libPythonQt_QtAll$${DEBUG_EXT}.a

7) src.pri:
Comment out 
win32:QMAKE_CXXFLAGS += /bigobj

8) Build the PythonQt.pro project in QtCreator

9) Copy PythonQt3.0/lib/PythonQt.dll and PythonQt3.0/lib/PythonQt_QtAll.dll to the camera module exectuable's directory


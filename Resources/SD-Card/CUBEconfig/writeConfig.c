
#include <string.h>
#include <stdio.h>
#include <windows.h>
#include "diskio.h"




/*-----------------------------------------------------------------------*/
/* Main                                                                  */


int main (int argc, char *argv[])
{
	unsigned char res;
  unsigned char mask;
  int pd, i, j, count;
  FILE *fp;
  unsigned char Buff[32768];			/* Working buffer */
  char str[100];

  
  // turn off output buffering
  setvbuf(stdout, 0, _IONBF, 0);
	printf("\n*** writeConfig 1.0 ***\n");

  if (argc > 2) {
    pd = atoi(argv[1]);
  
    if (!assign_drives(pd)) {
      printf("\nUsage: pcheck <phy drv#>\n");
      return 2;
    }


    sprintf(str,"%s",argv[2]);
    for (i = 0; i < 512; i++) {
      Buff[i] = 0;
    }
    fp = fopen(str, "r");
    if (fp != NULL) {
      printf("Overwriting card configuration data with data from file %s!\n", str);
      for (i = 0; i < 32; i++) {
        fscanf (fp, "%s", str);
        mask = 0;
        if (strlen(str) == 8) {
          for (j = 0; j < 8; j++) {
            mask = mask << 1;
            if (str[j] == '1')
              mask |= 0x01;
          }
        }
        Buff[i] = mask;
      }
      disk_write(0, Buff, 0, 1);
      fclose(fp);

      printf("\nNew configuration:\n");
      // read the configuration sector
      res = disk_read(0, Buff, 0, 1);
      if (res) {
        printf("rc=%d\n", (WORD)res);
        return 2;
      }

      count = 0;
      for (i = 0; i < 32; i++) {
        printf("Group %02d: ", i);
        if (Buff[i]) {
          printf("Card(s) ");
          for (j = 0; j < 8; j++) {
            if ((Buff[i] >> j) & 0x01) {
              count++;
              if (j == 0)
                printf("%d",j);
              else
                printf(",%d",j);
            }
          }
          printf(" enabled\n");
        }
      }
      printf("\n%d channels enabled, packet size = %d\n", count, 2*count+6);
    } else {
      printf("Cant open %s!\n", argv[2]);
    }
  } else {
    printf("Usage: writeConfig <disk> <file>\n");
  }
}
  



//#include <QCoreApplication>
#include "SyntheticDataGenerator.h"
#include <string.h>
//#include <QDebug>
//#include <QString>
#include <iostream>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <chrono>
#include <thread>

#define BUFSIZE 2048
#define TRODESHARDWARE_CONTROLPORT   8100
#define TRODESHARDWARE_DATAPORT      8200

#define STOP_ACQUISITION        0x62
#define START_ACQUISITION_NOECU 0x61
#define START_ACQUISITION_ECU   0x64
#define MCU_CHANNELS_0          0x00
#define MCU_CHANNELS_32         0x01
#define MCU_CHANNELS_64         0x03
#define MCU_CHANNELS_96         0x07
#define MCU_CHANNELS_128        0x0F
#define MCU_CHANNELS_160        0x1F


int main(int argc, char *argv[])
{
    //    QCoreApplication a(argc, argv);
    //    return a.exec();

    struct sockaddr_in controlAddr;         /* our address for control*/
    struct sockaddr_in dataAddr;            /* our address for data*/
    bool dataAddrInitialized = false;
    struct sockaddr_in clientAddr;          /* remote address */
    socklen_t addrlen = sizeof(clientAddr); /* length of addresses */
    int recvlen, sendlen;                   /* # bytes received */
    int fd;                                 /* our socket */

    fd_set readfds, preset_fds;             /* going to be used for select */
    struct timeval timeout;                 /* (me too) */

    char buf[BUFSIZE];                      /* receive buffer */

    bool streamingOn = false;

    SyntheticDataGenerator *dataGenerator = new SyntheticDataGenerator();


    double clock_period = (double) std::chrono::high_resolution_clock::period::num / (double) std::chrono::high_resolution_clock::period::den;


    std::chrono::nanoseconds period(1000000000/SAMPLING_RATE);

    std::cout << "High resolution clock period is " << clock_period <<
                ". Tick period is " << std::chrono::duration_cast<std::chrono::nanoseconds>(period).count() <<
                 std::endl;


    int counter = 0;

    /* create a UDP socket */
    if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        std::cout << "cannot create socket\n";
        return 0;
    }

    FD_ZERO(&preset_fds);
    FD_SET(fd, &preset_fds);
    timeout.tv_sec = 0;
    timeout.tv_usec = 10;

    /* bind the socket to any valid IP address and a specific port */

    memset((char *)&controlAddr, 0, sizeof(controlAddr));
    controlAddr.sin_family = AF_INET;
    controlAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    controlAddr.sin_port = htons(TRODESHARDWARE_CONTROLPORT);

    if (bind(fd, (struct sockaddr *)&controlAddr, sizeof(controlAddr)) < 0) {
        std::cout << "bind failed";
        return 0;
    }

    /* now loop, receiving data and printing what we received */
    std::cout << "waiting on port: " << TRODESHARDWARE_CONTROLPORT << std::endl;

    auto next = std::chrono::high_resolution_clock::now() + period;
    for (;;) {
        std::this_thread::sleep_until(next);
        next += period;

        // Check for messages from client
        memcpy(&readfds, &preset_fds, sizeof(fd_set));
        if (select(fd+1, &readfds, NULL, NULL, &timeout) < 0) {
            std::cout << "Error on select";
        }

        if (FD_ISSET(fd, &readfds)) {
            recvlen = recvfrom(fd, buf, BUFSIZE, 0, (struct sockaddr *)&clientAddr, &addrlen);

            if (recvlen > 2) {
                std::cout << "Strange message larger than 2 bytes received?";
            }
            else {
                switch(buf[0]) {
                case STOP_ACQUISITION :
                    buf[0] = 0x06;
                    std::cout << "received stop message";
                    sendlen = sendto(fd, buf, 1, 0, (struct sockaddr *)&clientAddr, addrlen);
                    if (sendlen == 1) {
                        std::cout << "Sent ACK response. Client port: " << ntohs(clientAddr.sin_port);
                        memcpy(&dataAddr, &clientAddr, addrlen);
                        dataAddr.sin_port = htons(TRODESHARDWARE_DATAPORT);
                        dataAddrInitialized = true;
                    }
                    else
                        std::cout << "Error sending ack response.";
                    streamingOn = false;
                    break;
                case START_ACQUISITION_NOECU :
                    dataGenerator->setHasECU(false); // default is true - roll through to next case
                case START_ACQUISITION_ECU :
                    streamingOn = true;
                    if (recvlen < 2) {
                        std::cout << "Start command missing HW setup byte";
                        streamingOn = false;
                    }
                    else {
                        if (buf[1] == MCU_CHANNELS_128)
                            dataGenerator->setNCHAN(128);
                        else if (buf[1] == MCU_CHANNELS_64)
                            dataGenerator->setNCHAN(64);
                        else if (buf[1] == MCU_CHANNELS_32)
                            dataGenerator->setNCHAN(32);
                        else if (buf[1] == MCU_CHANNELS_96)
                            dataGenerator->setNCHAN(96);
                        else if (buf[1] == MCU_CHANNELS_160)
                            dataGenerator->setNCHAN(160);
                        else if (buf[1] == MCU_CHANNELS_0)
                            dataGenerator->setNCHAN(0);
                        else {
                            std::cout << "Unrecognized HW setup byte";
                            streamingOn = false;
                        }

                        dataGenerator->resetTimestamp();
                        std::cout << "Starting acquisition NCHAN =" << dataGenerator->queryNCHAN() <<
                                    "with ECU? " << dataGenerator->queryHasECU();

                        break;
                default:
                            if (recvlen > 0) {
                                buf[recvlen] = 0;
                            }
                            std::cout << "received unknown message, " << recvlen << "bytes. Message: " << buf << std::endl;
                    }
                }
            }
        }

        dataGenerator->loadNextSample();
        if (streamingOn && dataAddrInitialized) {
            sendlen = sendto(fd, dataBuffer, bufferLen, 0, (struct sockaddr *)&dataAddr, addrlen);
            if (sendlen != bufferLen)
                std::cout << "Error sending data.";
        }

        counter++;
        if (counter >= 299999) {
            counter = 0;
            std::cout << "10 s";
        }

   }

   return 0;

}

#include "SyntheticDataGenerator.h"
#include <math.h>
#include <string.h>

#define ECU_HEADER_DATA_SIZE 42
#define NOECU_HEADER_DATA_SIZE 10

#define MAX_HEADER_DATA_SIZE 48
#define MAX_NCHAN 1024

char dataBuffer[MAX_HEADER_DATA_SIZE + sizeof(uint32_t) + MAX_NCHAN * sizeof(int16_t)];
int bufferLen;


SyntheticDataGenerator::SyntheticDataGenerator() :
    NCHAN(128),
    hasECU(true),
    waveFrequency(75),
    waveAmplitude(1000),
    waveModulatorFrequency(5),
    dioFrequency(3),
    dataIdx(0),
    timestamp(0)
{

    double dataPoint;

    for (int sampleIndex = 0; sampleIndex < DATA_LENGTH; sampleIndex++) {
        dataPoint = (double) sin(2 * M_PI * waveFrequency * ((double) sampleIndex/ SAMPLING_RATE));
        dataPoint *= (double) sin(2 * M_PI * waveModulatorFrequency * ((double) sampleIndex/ SAMPLING_RATE));
        dataPoint *= waveAmplitude;
        sourceData[sampleIndex] = (int16_t) dataPoint;

        dioData[sampleIndex] = ((unsigned char) (sin(2 * M_PI * dioFrequency * ((double) sampleIndex/ SAMPLING_RATE)) > 0)) * 0xFF;
    }

    bufferLen = 0;
}

void SyntheticDataGenerator::resetTimestamp()
{
    timestamp = 0;
}

int SyntheticDataGenerator::queryNCHAN()
{
    return NCHAN;
}

bool SyntheticDataGenerator::queryHasECU()
{
    return hasECU;
}


void SyntheticDataGenerator::setNCHAN(int value)
{
    NCHAN = value;
    if (hasECU)
        bufferLen = ECU_HEADER_DATA_SIZE + sizeof(uint32_t) + NCHAN * sizeof(int16_t);
    else
        bufferLen = NOECU_HEADER_DATA_SIZE + sizeof(uint32_t) + NCHAN * sizeof(int16_t);
}

void SyntheticDataGenerator::setHasECU(bool value)
{
    hasECU = value;
    if (hasECU)
        bufferLen = ECU_HEADER_DATA_SIZE + sizeof(uint32_t) + NCHAN * sizeof(int16_t);
    else
        bufferLen = NOECU_HEADER_DATA_SIZE + sizeof(uint32_t) + NCHAN * sizeof(int16_t);
}

void SyntheticDataGenerator::loadNextSample()
{

    /* Format of data UDP packet is:
     *
     *   (1) hardwareConf->headerSize uint16_t's comprising:
     *      (a) 1 char = 0x55
     *      (b) header data (potentially digital, analog, interleaved) - 41 bytes
     *   (2) uint32_t timestamp
     *   (3) NCHAN int16_t data samples
     *
     * */

    char *dPtr  = dataBuffer;
    int16_t *iPtr;
    uint32_t *tsPtr;

    *dPtr++ = 0x55;

    if (hasECU) {
        for (int i = 0; i < (ECU_HEADER_DATA_SIZE - 1); i++) {
            *dPtr++ = dioData[dataIdx];
        }
    }
    else {
        for (int i = 0; i < (NOECU_HEADER_DATA_SIZE - 1); i++) {
            *dPtr++ = dioData[dataIdx];
        }
    }

    tsPtr = (uint32_t *)dPtr;
    *tsPtr = timestamp++;
//    memcpy(dPtr, &timestamp, sizeof(uint32_t));
//    timestamp++;
    dPtr += sizeof(uint32_t);

    iPtr = (int16_t *)dPtr;
    for (int sampNum = 0; sampNum < NCHAN; sampNum++) {
        *iPtr++ = sourceData[dataIdx];
    }

    dataIdx = dataIdx + 1;
    if (dataIdx >= DATA_LENGTH)
        dataIdx = 0;
}



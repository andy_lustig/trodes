#ifndef SHAREDTRODESSTYLES_H
#define SHAREDTRODESSTYLES_H


#include <QDialog>
#include <QSlider>
#include <QLabel>
#include <QtWidgets>

class TrodesFont : public QFont {

public:
    TrodesFont();
};

class TrodesButton : public QPushButton {

Q_OBJECT

public:
    TrodesButton(QWidget *parent = 0);
    void setRedDown(bool yes);

};

#endif // SHAREDTRODESSTYLES_H

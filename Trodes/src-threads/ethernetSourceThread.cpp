/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ethernetSourceThread.h"
#include "globalObjects.h"

//On mac, set ethernet port to:
//IP: 192.168.0.1
//Subnet: 255.255.255.0

//On linux, set ethernet port to:
//IP:192.168.0.2 (or probably anything but .1)
//Subnet: 255.255.255.0


EthernetSourceRuntime::EthernetSourceRuntime(QObject *parent) {


}

EthernetSourceRuntime::~EthernetSourceRuntime() {
    udpDataSocket->close();
    udpDataSocket->deleteLater();

}


void EthernetSourceRuntime::Run() {

  udpDataSocket = new QUdpSocket(this);
  //Listing to port 8200 on all adresses
  bool connected  = udpDataSocket->bind(TRODESHARDWARE_DATAPORT);
  if (!connected) {
       qDebug() << "Error in socket binding";
       emit failure();
       emit finished();
       return;
  }

  packets = 0;


  //DWORD BytesReceived;
  //int PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + 1 + 1;
  PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + (2*hardwareConf->headerSize); //Packet size in bytes
  QByteArray datagram;
  datagram.resize(PACKET_SIZE);
  QHostAddress sender;
  quint16 senderPort;


  //unsigned char buffer[PACKET_SIZE];
  //buffer = new unsigned char[PACKET_SIZE];
  //buffer.resize(PACKET_SIZE);

  char *RxBuffer;
  int remainingSamples = 0;
  int leftInBuffer;
  double dTimestamp = 0.0;
  int tic = 0;
  int tempMax;
  int maxAvailable = 0;
  quitNow = false;
  acquiring = false;
  int numConsecErrors = 0;
  qDebug() << "Ethernet handle events loop running....";

  while (quitNow != true) {


      //Wait until a packet is available for reading.  This function blocks the
      //thread until data is available (or until the function times out)
      udpDataSocket->waitForReadyRead(500);

      //If data is available, read it one packet at a time
      while (udpDataSocket->hasPendingDatagrams()) {

          if (udpDataSocket->pendingDatagramSize() != PACKET_SIZE) {
              //if (aquiring) {
                  qDebug() << "Error in socket acquisition. Incoming packet size is: " << udpDataSocket->pendingDatagramSize() << " expected: " << PACKET_SIZE;

                  int errorPacketSize =  udpDataSocket->pendingDatagramSize();

                  //Read in the bad data from the buffer
                  while (errorPacketSize > 0) {
                        if (errorPacketSize >= (int) PACKET_SIZE) {
                            udpDataSocket->readDatagram(datagram.data(), datagram.size(),
                                              &sender, &senderPort);
                            errorPacketSize -= PACKET_SIZE;
                        } else {
                            udpDataSocket->readDatagram(datagram.data(), errorPacketSize,
                                              &sender, &senderPort);
                            errorPacketSize = 0;
                        }

                  }

                  numConsecErrors++;
                  if (numConsecErrors > 2000) {
                      //Here we should display a message that something has gone wrong,
                      //but perhaps not stop data acquisition altogether?

                      //quitNow = true;
                      //emit failure();
                      numConsecErrors = 0;
                  }
                  break;
              //}
          } else {




              //Process the received data packet.
              //The packet contains a 16-bit header for binary info,
              //a 32-bit time stamp, and hardwareConf->NCHAN 16-bit samples.

              leftInBuffer = PACKET_SIZE;

              udpDataSocket->readDatagram(datagram.data(), datagram.size(),
                                          &sender, &senderPort);


              RxBuffer = datagram.data();

              if (*RxBuffer != 0x55) {
                  //We have bad alignment.  Report error and toss packet.
                  qDebug() << "Bad frame alignment!";
                  int actualSyncLoc = 0;
                  for (unsigned int i = 0; i < PACKET_SIZE; i++) {
                      if (*(RxBuffer+i) == 0x55) {
                          actualSyncLoc = i;
                          qDebug() << "Sync byte at: " << actualSyncLoc;
                          break;
                      }
                  }
                  continue;
              }

              numConsecErrors = 0;
              packets++;


              //Read header info
              memcpy(&(rawData.digitalInfo[rawData.writeIdx*hardwareConf->headerSize]), \
                      RxBuffer, hardwareConf->headerSize*sizeof(uint16_t));

              RxBuffer += (2*hardwareConf->headerSize);
              leftInBuffer -= (2*hardwareConf->headerSize);


              //int16_t *dataPtr16 = (int16_t *)(RxBuffer);
              //rawData.digitalInfo[rawData.writeIdx] = *dataPtr16;
              //RxBuffer += 2;
              //leftInBuffer -= 2;

              //Process time stamp
              uint32_t* dataPtr = (uint32_t *)(RxBuffer);
              currentTimeStamp = *dataPtr;
              checkForCorrectTimeSequence();
              rawData.timestamps[rawData.writeIdx] = *dataPtr;
              RxBuffer += 4;
              leftInBuffer -= 4;
              dTimestamp += 1.0;
              rawData.dTime[rawData.writeIdx] = dTimestamp;
              if (benchConfig->isRecordingSysTime()) {
                  rawData.sysTimestamps[rawData.writeIdx] = QTime::currentTime().msecsSinceStartOfDay();
              }

              //Process the data in the packet
              remainingSamples = hardwareConf->NCHAN;
              memcpy(&(rawData.data[rawData.writeIdx*hardwareConf->NCHAN]), \
                      RxBuffer, remainingSamples*sizeof(uint16_t));
              RxBuffer += remainingSamples * 2;
              leftInBuffer -= remainingSamples * 2;
              remainingSamples = 0;


              //Advance the write markers and release a semaphore

              writeMarkerMutex.lock();
              rawData.writeIdx = (rawData.writeIdx + 1) % EEG_BUFFER_SIZE;
              rawDataWritten++;
              writeMarkerMutex.unlock();

              for (int a = 0; a < rawDataAvailable.length(); a++) {
                  rawDataAvailable[a]->release(1);
              }
              //rawDataAvailableForSave.release(1);



              if ((tempMax = rawDataAvailable[0]->available()) > maxAvailable)
                  maxAvailable = tempMax;

              if ((++tic % 10000) == 0) {
                  maxAvailable = 0;
              }
          }
      }

  }

  qDebug() << "Ethernet loop finished.";
  emit finished();


}


EthernetInterface::EthernetInterface(QObject *) {
  state = SOURCE_STATE_NOT_CONNECTED;
  UDPDataProcessor = NULL;

}

EthernetInterface::~EthernetInterface() {


}

void EthernetInterface::InitInterface() {


  connectErrorThrown = false;
  udpControlSocket = new QUdpSocket(this);
  //udpControlSocketReceiver = new QUdpSocket(this);
  QHostAddress tmpAddress;
  uint tmpPort;
  if (networkConf != NULL) {
    qDebug() << networkConf->hardwareAddress <<networkConf->hardwarePort ;
    tmpAddress.setAddress(networkConf->hardwareAddress);
    tmpPort = networkConf->hardwarePort;
  } else {
      tmpAddress.setAddress(TRODESHARDWARE_DEFAULTIP);
      tmpPort = TRODESHARDWARE_CONTROLPORT;
  }
  if (!udpControlSocket->bind(QHostAddress::Any,udpControlSocket->localPort())) {
      qDebug() << "Error binding to control port";
      return;
  }


  //Using a SharedAddress setting does not work on mac or windows!!!!
  /*
  if (!udpControlSocket->bind(QHostAddress::Any,udpControlSocket->localPort() ,QUdpSocket::ShareAddress)) {
      qDebug() << "Error binding to control port";
      return;
  }*/


  QByteArray datagram;
  datagram.resize(1);

  //datagram[0] = 0x01; //dummy byte until we have the command byte working
  datagram[0] = 0x62; // 0x62 = stop data capture

  // send stop capture command in order to recieve ack message
  //Windows allows writing in broadcast mode, but Macs do not.  Not sure about linux yet.
#ifdef WIN32
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                          QHostAddress::Broadcast, tmpPort);
#else

  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                            tmpAddress, tmpPort);
#endif


  if (!udpControlSocket->waitForReadyRead(1000)) {
      qDebug() << "Open UDP communication failed";
      connectErrorThrown = true;      
      CloseInterface();
      emit stateChanged(SOURCE_STATE_CONNECTERROR);
      return;
  }


  connect(udpControlSocket,SIGNAL(readyRead()),this,SLOT(controlSocketAcknowledgeReceived()));

  //udpControlSocket->connectToHost(tmpAddress,TRODESHARDWARE_CONTROLPORT);


  //initialization went ok, so start the runtime thread
  UDPDataProcessor = new EthernetSourceRuntime(NULL);
  setUpThread(UDPDataProcessor);

  /*
  workerThread = new QThread();
  UDPDataProcessor->moveToThread(workerThread);
  //connect(workerThread, SIGNAL(started()), usbDataProcessor, SLOT(Run()));
  UDPDataProcessor->connect(this, SIGNAL(startRuntime()), SLOT(Run()));
  connect(UDPDataProcessor, SIGNAL(finished()), workerThread, SLOT(quit()));
  connect(UDPDataProcessor, SIGNAL(finished()), UDPDataProcessor, SLOT(deleteLater()));
  connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
  workerThread->start();
  */

  state = SOURCE_STATE_INITIALIZED;

  emit stateChanged(SOURCE_STATE_INITIALIZED);

  //GetHeadstageSettings();

}

void EthernetInterface::StartAcquisition(void) {
  static int runtimeStarted  = 0;
  //unsigned char TxBuffer[256]; // Contains data to write to device


  //Each command will eventually start with a 'type' byte, which will designate
  //what the following data is, and how many bytes are being sent. For now, no type byte exists.

  QByteArray datagram;
  datagram.resize(2);

  rawData.writeIdx = 0; // location where we're currently writing

  // Send "start data capture" command

  switch (hardwareConf->NCHAN) {
    case 0:
      datagram[1] = 0x00; // only card 0 enabled = 32 channels
      break;
    case 32:
      datagram[1] = 0x01; // only card 0 enabled = 32 channels
      break;
    case 64:
      datagram[1] = 0x03; // card 0 and 1 enabled = 64 channels
      break;
    case 96:
      datagram[1] = 0x07; // card 0,1,and 2 enabled = 96 channels
      break;
    case 128:
      datagram[1] = 0x0F; // card 0,1,2, and 3 enabled = 128 channels
      break;
    case 160:
      datagram[1] = 0x1F; // card 0,1,2,3, and 4 enabled = 160 channels
      break;
    default:
      datagram[1] = 0x01; // default mode: only card 0 enabled = 32 channels
      break;

  }

  //datagram[1] = 0x61; // 0x61 = start data capture without ECU
  //datagram[1] = 0x64; // 0x64 = start data capture with ECU

  datagram[0] = startCommandValue;
  //nsUSBRuntime->aquiring = true;
  UDPDataProcessor->acquiring = true;

  // send start capture command
#ifdef WIN32
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                          QHostAddress::Broadcast, networkConf->hardwarePort);
#else
  QHostAddress tmpAddress;
  tmpAddress.setAddress(networkConf->hardwareAddress);
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                            tmpAddress, networkConf->hardwarePort);

  //udpControlSocket->write(datagram);

  //QThread::msleep(100);

#endif


  if (runtimeStarted == 0) {
    emit startRuntime();
    qDebug() << "Told runtime to start";
  }

  emit acquisitionStarted();
  state = SOURCE_STATE_RUNNING;
  emit stateChanged(SOURCE_STATE_RUNNING);
}

void EthernetInterface::StartSimulation() {
  static int runtimeStarted  = 0;
  //unsigned char TxBuffer[256]; // Contains data to write to device


  //Each command will eventually start with a 'type' byte, which will designate
  //what the following data is, and how many bytes are being sent. For now, no type byte exists.

  qDebug() << "Sending simulation command. Aux bytes:" << (hardwareConf->headerSize*2)-2 << "Num channel banks:" << ((hardwareConf->NCHAN/32)-1);

  QByteArray datagram;
  datagram.resize(3);
  QDataStream msg(&datagram, QIODevice::ReadWrite);
  msg.setByteOrder(QDataStream::LittleEndian);
  msg << (quint8)command_startSimulation;
  msg << (quint8)((hardwareConf->headerSize*2)-2); //Number of auxilliary bytes to add to the packet
  msg << (quint8)((hardwareConf->NCHAN/32)-1);

  rawData.writeIdx = 0; // location where we're currently writing


  UDPDataProcessor->acquiring = true;

  // send start capture command
#ifdef WIN32
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                          QHostAddress::Broadcast, networkConf->hardwarePort);
#else
  QHostAddress tmpAddress;
  tmpAddress.setAddress(networkConf->hardwareAddress);
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                            tmpAddress, networkConf->hardwarePort);


#endif


  if (runtimeStarted == 0) {
    emit startRuntime();
    qDebug() << "Told runtime to start";
  }

  emit acquisitionStarted();
  state = SOURCE_STATE_RUNNING;
  emit stateChanged(SOURCE_STATE_RUNNING);
}


void EthernetInterface::StopAcquisition(void) {
  //unsigned char TxBuffer[256]; // Contains data to write to device


  QByteArray datagram;
  datagram.resize(1);

  //datagram[0] = 0x01; //dummy byte until we have the command byte working
  datagram[0] = command_stop; // 0x62 = stop data capture

  // send stop capture command
  //Windows allows writing in broadcast mode, but Macs do not.  Not sure about linux yet.
#ifdef WIN32
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                          QHostAddress::Broadcast, networkConf->hardwarePort);
#else
  QHostAddress tmpAddress;
  tmpAddress.setAddress(networkConf->hardwareAddress);
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                            tmpAddress, networkConf->hardwarePort);
  udpControlSocket->flush();
  //udpControlSocket->write(datagram);
#endif



  UDPDataProcessor->quitNow = true;
  udpControlSocket->close();
  udpControlSocket->deleteLater();
  QThread::msleep(100);

  if (!connectErrorThrown) {
    InitInterface();
  } else {
    CloseInterface();
  }

 //----------------------------

  emit acquisitionStopped();
  state = SOURCE_STATE_INITIALIZED;
  emit stateChanged(SOURCE_STATE_INITIALIZED);
  UDPDataProcessor->acquiring = false;
}

void EthernetInterface::CloseInterface(void) {

  if (state == SOURCE_STATE_INITIALIZED || connectErrorThrown) {


    if (UDPDataProcessor != NULL) {
        UDPDataProcessor->quitNow = true;
    }

    udpControlSocket->close();
    udpControlSocket->deleteLater();


    emit stateChanged(SOURCE_STATE_NOT_CONNECTED);



  } else if (state == SOURCE_STATE_RUNNING) {
      StopAcquisition();
     // CloseInterface();
  }

}

quint64 EthernetInterface::getTotalDroppedPacketEvents() {
    quint64 rVal = 0;
    if (UDPDataProcessor != NULL) {
        rVal = UDPDataProcessor->totalDroppedPacketEvents;
    }
    return rVal;
}

void EthernetInterface::GetHeadstageSettings() {
    QByteArray datagram;
    datagram.resize(1);
    datagram[0] = command_getHeadstageSettings;
    sendMessageToHardware(datagram);
    qDebug() << "Requested headstage settings.";

}

void EthernetInterface::SendHeadstageSettings(HeadstageSettings s) {

    /*
    //Send autosettle number of channels above threshold
    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);
    msg << (quint8)0x82; // 0x82 = set autosettle num channels
    if (s.autoSettleOn) {
        msg << (uint16_t)((100.0/s.percentChannelsForSettle)*hardwareConf->NCHAN);
    } else {
        //More channels than there actually are
        msg << (uint16_t)(hardwareConf->NCHAN + 1);
    }
    sendMessageToHardware(datagram);

    //Next send autosettle threshold
    QByteArray datagram2;
    datagram2.resize(3);
    QDataStream msg2(&datagram2, QIODevice::ReadWrite);
    msg2.setByteOrder(QDataStream::LittleEndian);

    msg2 << (quint8)0x81; // 0x82 = set autosettle thresh
    msg2 << (uint16_t)s.threshForSettle;
    sendMessageToHardware(datagram2);
    */




    //SET HEADSTAGE SETTINGS (MESSAGE TO HARDWARE)-- 8 BYTES
    //<0x82><uint8 autoSettleOn><uint16 autosettle percent of channels><uint16 autosettle thresh><uint8 smartRefOn><unint8 sensors on>
    //sensors on is an 8-bit code, least significant bit last <unused unused unused unused unused mag gyro accel>
    QByteArray datagram;
    datagram.resize(8);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);
    msg << (quint8)command_setHeadstageSettings;
    msg << (uint8_t)s.autoSettleOn;
    if (s.autoSettleOn) {
        //msg << (uint16_t)((100.0/s.percentChannelsForSettle)*hardwareConf->NCHAN); //convert to number of channels
        msg << s.percentChannelsForSettle;
    } else {
        //More channels than there actually are
        //msg << (uint16_t)(hardwareConf->NCHAN + 1);
        msg << (uint16_t)100;
    }
    msg << (uint16_t)s.threshForSettle;

    msg << (uint8_t)s.smartRefOn;


    uint8_t sensorsOn = 0;
    sensorsOn |= ((s.accelSensorOn & 1) << 0);
    sensorsOn |= ((s.gyroSensorOn & 1) << 1);
    sensorsOn |= ((s.magSensorOn & 1) << 2);
    msg << sensorsOn;


    sendMessageToHardware(datagram);



}

void EthernetInterface::SendFunctionTrigger(int funcNum) {
    if (state == SOURCE_STATE_RUNNING) {


        QByteArray datagram;
        datagram.resize(3);
        QDataStream msg(&datagram, QIODevice::ReadWrite);
        msg.setByteOrder(QDataStream::LittleEndian);
        msg << (quint8)command_trigger; // 0x69 = trigger command
        msg << (uint16_t)(funcNum);
        //msg << 0x01;

        sendMessageToHardware(datagram);






        /*QByteArray datagram;
        datagram.resize(1);

        //datagram[0] = 0x01; //dummy byte until we have the command byte working
        datagram[0] = 0x69; // 0x69 = trigger command
        datagram[1] = 1;

        sendMessageToHardware(datagram);

        datagram[0] = 0x69; // 0x69 = trigger command
        datagram[1] = 0;
        sendMessageToHardware(datagram);*/
    }
}

void EthernetInterface::SendSettleCommand() {
    if (state == SOURCE_STATE_RUNNING) {
        qDebug() << "Sending settle command.";
        QByteArray datagram;
        datagram.resize(1);

        //datagram[0] = 0x01; //dummy byte until we have the command byte working
        datagram[0] = (quint8)command_settle; // 0x66 = settle command

        sendMessageToHardware(datagram);
    }
}

void EthernetInterface::SendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState) {
    if ((state == SOURCE_STATE_RUNNING)||(state == SOURCE_STATE_INITIALIZED)) {

        QString tStateString;
        if (triggerState == 0) {
            tStateString = "Off";
        } else if (triggerState == 1) {
            tStateString = "Rising edge";
        } else if (triggerState == 2) {
            tStateString = "Falling edge";
        } else if (triggerState == 3) {
            tStateString = "Both upward and downward edge";
        }

        qDebug() << "Sending settle channel." << byteInPacket << "Byte" << bit << "Bit" << tStateString;

        QByteArray datagram;
        datagram.resize(7);
        QDataStream msg(&datagram, QIODevice::ReadWrite);
        msg.setByteOrder(QDataStream::LittleEndian);
        msg << (quint8)command_setSettleTriggerChannel; // 0x6A = set settle channel
        msg << (uint16_t)(byteInPacket); //the byte in the packet to use
        msg << bit; //uint8, the bit within the given byte to use (0-7)
        msg << (uint16_t)(delay); //delay for the settle to occur in number of samples
        msg << triggerState; //uint8 (0 for off, 1 for downward edge, 2 for upward edge)

        sendMessageToHardware(datagram);
    }
}

void EthernetInterface::SendSDCardUnlock() {
    qDebug() << "Sending SD card unlock command.";
    QByteArray datagram;
    datagram.resize(1);

    //datagram[0] = 0x01; //dummy byte until we have the command byte working
    datagram[0] = (quint8)command_sdCardUnlock; // 0x65 = SD card unlock

    sendMessageToHardware(datagram);

}

void EthernetInterface::ConnectToSDCard() {
    qDebug() << "Sending SD card connect command.";
    QByteArray datagram;
    datagram.resize(1);

    //datagram[0] = 0x01; //dummy byte until we have the command byte working
    datagram[0] = (quint8)command_connectToSD; // 0x67 = SD card unlock

    sendMessageToHardware(datagram);

}

void EthernetInterface::ReconfigureSDCard(int numChannels) {
    qDebug() << "Sending SD card reconfigure command.";
    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);
    //msg << 0x01; //dummy byte until we have the command byte working
    msg << (quint8)command_configureSD; // 0x68 = ping the MCU for the card
    msg << (uint16_t)numChannels;

    sendMessageToHardware(datagram);
}

void EthernetInterface::sendMessageToHardware(QByteArray datagram) {
    //Windows allows writing in broadcast mode, but Macs do not.  Not sure about linux yet.
    QHostAddress tmpAddress;
    quint16 tmpPort;
    if (networkConf != NULL) {

      tmpAddress.setAddress(networkConf->hardwareAddress);
      tmpPort = networkConf->hardwarePort;
    } else {
        tmpAddress.setAddress(TRODESHARDWARE_DEFAULTIP);
        tmpPort = TRODESHARDWARE_CONTROLPORT;
    }

  #ifdef WIN32
    udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                            QHostAddress::Broadcast, tmpPort);
  #else

    udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                              tmpAddress, tmpPort);
    udpControlSocket->flush();

  #endif
}


void EthernetInterface::controlSocketAcknowledgeReceived() {
    char inputarray[20];
    //qDebug() << "Got a response";
    while (udpControlSocket->hasPendingDatagrams()) {
        udpControlSocket->readDatagram(inputarray,20);

        if (inputarray[0] == received_headstageSettings) {
            qDebug() << "Headstage settings returned from hardware.";
            HeadstageSettings s;

            //HEADSTAGE SETTINGS MESSAGE FROM HARDWARE  -- 15 BYTES
            //<0x81><uint16 number of channels><uint16 id code><uint16 config code><uint8 autoSettleMode><uint16 autosettle number of channels><uint16 autosettle thresh><uint8 smartRefMode><unint8 sensors available><unint8 sensors on>
            //autoSettleMode and smartRefMode use the two least sig bits to signal availability (bit 1) and on/off state (bit 0)
            //sensors on/available are 8-bit codes, least significant bit last <unused unused unused unused unused mag gyro accel>
            QByteArray datagram(inputarray+1);
            QDataStream msg(&datagram, QIODevice::ReadWrite);
            msg.setByteOrder(QDataStream::LittleEndian);

            msg >> s.numberOfChannels; //2 bytes
            msg >> s.hsTypeCode; //2 bytes
            msg >> s.hsConfigCode; //2 bytes
            uint8_t tempModeHolder;
            msg >> tempModeHolder; //1 byte
            s.autosettleAvailable = (tempModeHolder & 2);
            s.autoSettleOn = (tempModeHolder & 1);

            msg >> s.percentChannelsForSettle; //2 bytes
            msg >> s.threshForSettle; //2 bytes

            msg >> tempModeHolder; //1 byte
            s.smartRefAvailable = (tempModeHolder & 2);
            s.smartRefOn = (tempModeHolder & 1);

            msg >> tempModeHolder; //1 byte (sensors available bit code)
            s.accelSensorAvailable = (tempModeHolder & (1 << 0));
            s.gyroSensorAvailable = (tempModeHolder & (1 << 1));
            s.magSensorAvailable = (tempModeHolder & (1 << 2));

            msg >> tempModeHolder; //1 byte (sensors on bit code)
            s.accelSensorOn = (tempModeHolder & (1 << 0));
            s.gyroSensorOn = (tempModeHolder & (1 << 1));
            s.magSensorOn = (tempModeHolder & (1 << 2));

            emit headstageSettingsReturned(s);
        }
    }

}



/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "sourceController.h"
#include "globalObjects.h"

//The SourceController object acts as a wrapper for all threads that aquire data from a data stream.
//The rest of the code can interact with this wrapper regardless of which source is currenly active.

uint32_t currentTimeStamp = 0;

eegDataBuffer rawData;
QAtomicInteger<quint64> rawDataWritten = 0; //global variable in globalObjects.h
QList <QSemaphore *> rawDataAvailable; //For the stream processors
QSemaphore rawDataAvailableForSave;
QMutex rawDataAvailableMutex;
QMutex writeMarkerMutex;
QString playbackFile;
bool playbackFileOpen = false;
int fileDataPos;
int playbackFileSize;
int playbackFileCurrentLocation;
int filePlaybackSpeed;  // 1 is actual speed, 2 is 2X, etc.
extern bool unitTestMode;


//---------------------------------------
//SourceController

SourceController::SourceController(QObject *) {
  state = 0;
  currentSource = SourceNone;
  USBSource = NULL;
  waveGeneratorSource = NULL;
  spikesGeneratorSource = NULL;
  fileSource = NULL;
  ethernetSource = NULL;
  currentSourceObj = NULL;

}

void SourceController::clearBuffers() {


    for (int i=0;i < EEG_BUFFER_SIZE*256; i++) {
        rawData.data[i] = 0;
    }

    for (int i=0;i < EEG_BUFFER_SIZE*8; i++) {
        rawData.digitalInfo[i] = 0;
    }
    for (int i=0;i < EEG_BUFFER_SIZE; i++) {
        rawData.timestamps[i] = 0;
        if (benchConfig->isRecordingSysTime()) {
            rawData.sysTimestamps[i] = 0;
        }
        rawData.dTime[i] = 0;
    }
    rawData.writeIdx = 0;

}

quint64 SourceController::getTotalDroppedPacketEvents() {
    quint64 rVal = 0;
    if (currentSourceObj != NULL) {
        rVal = currentSourceObj->getTotalDroppedPacketEvents();
    }
    return rVal;
}

void SourceController::disconnectFromSource() {

    if (currentSourceObj != NULL) {
        currentSourceObj->StopAcquisition();
    }

    writeMarkerMutex.lock();
    rawDataWritten = 0; //for sound and save threads
    writeMarkerMutex.unlock();

    //There might be available semaphores, so we aquire them up

    for (int a = 0; a < rawDataAvailable.length(); a++) {
        while (rawDataAvailable[a]->available() > 0) {
            rawDataAvailable[a]->tryAcquire();
        }        
    }

    //Make sure the current time is reset
    currentTimeStamp = 0;
    clearBuffers();

}

void SourceController::connectToSource_Simulation() {

    numConnectionTries = 0;
    if (currentSourceObj != NULL) {
        currentSourceObj->StartSimulation();
    }

}

void SourceController::connectToSource() {

    numConnectionTries = 0;
    if (currentSourceObj != NULL) {
        currentSourceObj->StartAcquisition();
    }

}

void SourceController::setSource(DataSource source) {


    //first we kill the current source thread
    if (currentSourceObj != NULL) {
        disconnect(this,SLOT(setSourceState(int)));
        currentSourceObj->CloseInterface();

        //QThread::msleep(200);
        delete currentSourceObj;
        //currentSourceObj->deleteLater();
    }
    writeMarkerMutex.lock();
    rawDataWritten = 0;
    writeMarkerMutex.unlock();
    currentSource = source;

    //then we start the new one
    switch (source) {
    case SourceNone:
        //No Source

        currentSourceObj = NULL;
        emit stateChanged(SOURCE_STATE_NOT_CONNECTED);
        break;
    case SourceFake:
        //generate fake data
        waveGeneratorSource = new simulateDataInterface(NULL);
        currentSourceObj = waveGeneratorSource;
        break;
    case SourceFakeSpikes:
        //generate fake data
        spikesGeneratorSource = new simulateSpikesInterface(NULL);
        currentSourceObj = spikesGeneratorSource;
        break;
    case SourceFile:
        //stream from file
        fileSource = new fileSourceInterface(NULL);
        currentSourceObj = fileSource;

        break;
    case SourceEthernet:
        //streaming via ethernet
        currentSourceObj = new EthernetInterface(NULL);
        if (hardwareConf != NULL) {
            currentSourceObj->setECUConnected(hardwareConf->ECUConnected);
        }
        break;
    case SourceUSBDAQ:
        //streaming via USB
        currentSourceObj = new USBDAQInterface(NULL);
        if (hardwareConf != NULL) {
            currentSourceObj->setECUConnected(hardwareConf->ECUConnected);
        }
        break;
    case SourceRhythm:
#ifdef RHYTHM
        //streaming via USB with Rhythm API
        currentSourceObj = new RhythmInterface(NULL);
        if (hardwareConf != NULL) {
            currentSourceObj->setECUConnected(hardwareConf->ECUConnected);
        }

#endif
        break;
    }

    if (source != SourceNone) {
        connect(currentSourceObj,SIGNAL(stateChanged(int)),this,SLOT(setSourceState(int)));
        connect(currentSourceObj,SIGNAL(headstageSettingsReturned(HeadstageSettings)),this,SLOT(newHeadstageSettings(HeadstageSettings)));
        currentSourceObj->InitInterface();
        connect(currentSourceObj, SIGNAL(acquisitionStarted()),this,SLOT(StartAcquisition()));
        connect(currentSourceObj,SIGNAL(acquisitionStopped()),this,SLOT(StopAcquisition()));
        connect(currentSourceObj,SIGNAL(SDCardStatus(bool,int,bool,bool)),this,SIGNAL(SDCardStatus(bool,int,bool,bool)));

    }

}

void SourceController::pauseSource() {

    if (currentSource == SourceFile) {
        fileSource->PauseAcquisition();
    }
    setSourceState(SOURCE_STATE_PAUSED);
}

void SourceController::StartAcquisition(void) {
    emit acquisitionStarted(); //tells display thread to start streaming
}

void SourceController::PauseAcquisition(void) {
    writeMarkerMutex.lock();
    rawDataWritten = 0; //for sound and save threads
    writeMarkerMutex.unlock();

    //There might be available semaphores, so we aquire them up
    /*for (int a = 0; a < rawDataAvailable.length(); a++) {
        while (rawDataAvailable[a]->available() > 0) {
            rawDataAvailable[a]->tryAcquire();
        }
    }*/

    emit acquisitionPaused();
}

void SourceController::StopAcquisition(void) {

    writeMarkerMutex.lock();
    rawDataWritten = 0; //for sound
    writeMarkerMutex.unlock();

    //There might be available semaphores, so we aquire them up


    /*for (int a = 0; a < rawDataAvailable.length(); a++) {
        while (rawDataAvailable[a]->available() > 0) {
            rawDataAvailable[a]->tryAcquire();
        }
    }*/

    //Make sure the current time is reset
    currentTimeStamp = 0;

    //Make sure the current time is reset (unless we have paused a playback file)
    /*
    if (currentSource == 2) {
        if (!fileSource->filePaused) {
            currentTimeStamp = 0;
        }
    } else {
        currentTimeStamp = 0;
    }*/

    emit acquisitionStopped(); //tells display thread to stop streaming
}

void SourceController::newHeadstageSettings(HeadstageSettings s) {

    emit headstageSettingsReturned(s);
}

void SourceController::sendFunctionTriggerCommand(int funcNum) {
    if (currentSourceObj != NULL) {
        currentSourceObj->SendFunctionTrigger(funcNum);
    }
}

void SourceController::sendSettleCommand() {
    if (currentSourceObj != NULL) {
        currentSourceObj->SendSettleCommand();
    }
}

void SourceController::sendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState) {
    if (currentSourceObj != NULL) {
        currentSourceObj->SendSettleChannel(byteInPacket, bit, delay, triggerState);
    }
}

void SourceController::setHeadstageSettings(HeadstageSettings s) {
    currentHSSettings = s;
    if (currentSourceObj != NULL) {
        currentSourceObj->SendHeadstageSettings(currentHSSettings);
    }
}

void SourceController::getHeadstageSettings() {
    if (currentSourceObj != NULL) {
        currentSourceObj->GetHeadstageSettings();
    }
}

void SourceController::connectToSDCard() {
    if (currentSourceObj != NULL) {
        currentSourceObj->ConnectToSDCard();
    }
}

void SourceController::enableSDCard() {
    if (currentSourceObj != NULL) {
        currentSourceObj->SendSDCardUnlock();
    }
}

void SourceController::reconfigureSDCard(int numChannels) {
    if (currentSourceObj != NULL) {
        currentSourceObj->ReconfigureSDCard(numChannels);
    }
}

void SourceController::dataError() {

    numConnectionTries++;

    if (numConnectionTries < 3) {
        StopAcquisition();
        qDebug() << "Data coming in wrong.... retrying connection.";
        QThread::msleep(100);
        StartAcquisition();
    } else {
        StopAcquisition();

    }

}

void SourceController::noDataComing() {
    qDebug() << "No data coming from hardware. Please reset the system and try again.";
    if (!unitTestMode) {
        if (currentSource != SourceFile) {

            QMessageBox messageBox;
            messageBox.critical(0,"Error","No data coming from hardware. Please reset the system and try again.");
            messageBox.setFixedSize(500,200);
            //disconnectFromSource();
        }
    }
}

void SourceController::setSourceState(int state) {
    //pass the state along to the mainWindow
    emit stateChanged(state);
}

void SourceController::waitForThreads() {
    if (currentSource == 2) {
        //The source is a file
        fileSource->waitForThreads();
    }
}

//This object is no longer being used.  Copying data to this intermediate object causes a huge hit on performance.
RawDataSample::RawDataSample(uint32_t ts, double dt)
{
  for (int n = 0; n < spikeConf->ntrodes.length(); n++) {
    neuralData.append(QVector<int16_t>(spikeConf->ntrodes.at(n)->hw_chan.length()));
  }
  for (int i = 0; i < headerConf->headerChannels.length(); i++) {
    headerData.append(0);
  }
  timestamp = ts;
  dTime = dt;
}

import logging
import os.path
import json
import struct
from collections import OrderedDict
import pandas as pd
from abc import ABCMeta, abstractmethod


class BinaryRecordsError(Exception):
    def __init__(self, value, **kwargs):
        self.value = value
        self.data = kwargs

    def __str__(self):
        return repr(self.value) + '\n' + repr(self.data)


class BinaryRecordBase(metaclass=ABCMeta):
    def __init__(self, rec_id, rec_labels, rec_format, rec_writer=None):
        self.rec_id = rec_id
        self.rec_labels = rec_labels
        self.rec_format = rec_format
        self.rec_writer = rec_writer
        self.rec_writer_enabled = False

    def set_record_writer(self, rec_writer):
        self.rec_writer = rec_writer

    def start_record_writing(self):
        if self.rec_writer and not self.rec_writer.closed:
            self.rec_writer_enabled = True
        else:
            self.class_log.warning('Can\'t start recording, file not open!')

    def stop_record_writing(self):
        self.rec_writer_enabled = False

    def write_record(self, *args):
        if self.rec_writer_enabled and not self.rec_writer.closed:
            self.rec_writer.write_rec(*args)


class BinaryRecordsManager:
    """Managers FSData's file records"""
    def __init__(self, label, save_dir, file_prefix, file_postfix):
        self.class_log = logging.getLogger(name='{}.{}'.format(self.__class__.__module__, self.__class__.__name__))

        self._label = label
        self._save_dir = save_dir
        self._file_prefix = file_prefix
        self._file_postfix = file_postfix
        self._rec_format_dict = {}
        self._rec_label_dict = {}
        self._next_file_index = 1
        self._files = {}

    @property
    def file_prefix(self):
        return self._file_prefix

    @file_prefix.setter
    def file_prefix(self, file_prefix):
        if self._next_file_index >= 2:
            raise BinaryRecordsError('Cannot modify filename after manager has created a file!')
        else:
            self._file_prefix = file_prefix

    @property
    def label(self):
        return self._label

    @label.setter
    def label(self, label):
        if self._next_file_index >= 2:
            raise BinaryRecordsError('Cannot modify label after manager has created a file!')
        else:
            self._label = label

    def register_rec_type(self, rec_id, rec_labels, rec_struct_fmt):
        if self._next_file_index >= 2:
            raise BinaryRecordsError('Cannot add more record types after manager has created a file!')
        else:
            if rec_id in self._rec_format_dict:
                self.class_log.warning('Record ID already exists, overwriting old rec ({}:{}) with new ({}:{})'.
                                       format(rec_id, self._rec_format_dict[rec_id], rec_id, rec_struct_fmt))
            self._rec_format_dict.update({rec_id: rec_struct_fmt})
            self._rec_label_dict.update({rec_id: rec_labels})

    def new_writer(self):
        new_bin_writer = BinaryRecordsFileWriter(label=self._label, file_id=self._next_file_index,
                                                 save_dir=self._save_dir, file_prefix=self._file_prefix,
                                                 file_postfix=self._file_postfix, rec_label_dict=self._rec_label_dict,
                                                 rec_format_dict=self._rec_format_dict)

        self._files[self._next_file_index] = new_bin_writer
        self._next_file_index += 1
        return new_bin_writer

    def close(self):
        for file_id, file in self._files.items():
            file.close()


class BinaryRecordsFileWriter:
    """File handler for a single Binary Records file.

    A Binary Records file consists of a JSON header prepended to the file that must define the following entries:
        file_prefix: The root file name that is shared if a data store spans multiple files
        file_id: A unique ID for the given file
        name: Descriptive label (shared across all files)
        rec_type_spec: The format (python struct - format character) of all possible recs

    What follows is a binary blob that contains a list of records with the following format:
        <rec_ind (uint32)> <rec_type (uint8)> <rec_data>

    Each record type with unique ID must be specified in rec_type_spec using python struct's format character syntax
    (don't prepend with a byte order character).

    Each record type has a fixed size that is implicitly defined by its format string.

    """
    def __init__(self, label, file_id, save_dir, file_prefix, file_postfix, rec_label_dict, rec_format_dict):
        self._label = label
        self._file_id = file_id
        self._save_dir = save_dir
        self._file_prefix = file_prefix
        self._file_path = os.path.join(save_dir, '{}.{:02d}.{}'.format(file_prefix, file_id, file_postfix))
        self._file_handle = open(self._file_path, 'wb')
        self._rec_label_dict = rec_label_dict
        self._rec_format_dict = rec_format_dict
        self._header_json = json.dumps(OrderedDict([('file_prefix', file_prefix),
                                                    ('file_id', file_id),
                                                    ('label', label),
                                                    ('rec_formats', rec_format_dict),
                                                    ('rec_labels', rec_label_dict)]))

        # write header to file
        self._file_handle.write(bytearray(self._header_json, encoding='utf-8'))

        self._rec_counter = 0

    @property
    def rec_format_dict(self):
        return self._rec_format_dict

    @rec_format_dict.setter
    def rec_format_dict(self, rec_format_dict):
        raise BinaryRecordsError('Cannot modify rec_format_dict after object has been created!')

    def write_rec(self, rec_type_id, *args):
        try:
            rec_bytes = struct.pack('=IB' + self.rec_format_dict[rec_type_id], self._rec_counter, rec_type_id, *args)
            self._file_handle.write(rec_bytes)
            self._rec_counter += 1
        except struct.error as ex:
            raise BinaryRecordsError('Data does not match record {}\'s data format.'.format(rec_type_id),
                                     rec_type_id=rec_type_id, rec_type_fmt=self.rec_format_dict[rec_type_id],
                                     rec_data=args, orig_error=ex) from ex

    def __del__(self):
        self._file_handle.close()

    @property
    def closed(self):
        return self._file_handle.closed

    def close(self):
        self._file_handle.close()


class BinaryRecordsFileReader:
    def __init__(self, file_id, save_dir, file_prefix, file_postfix):
        self._file_id = file_id
        self._save_dir = save_dir
        self._file_prefix = file_prefix
        self._file_path = os.path.join(save_dir, '{}.{:02d}.{}'.format(file_prefix, file_id, file_postfix))
        self._file_handle = open(self._file_path, 'rb')

        self._header_bytes = None
        self._data_start_byte = None
        self._extract_json_header()
        self._header = json.loads(self._header_bytes.decode('utf-8'))

    def _extract_json_header(self):
        self._file_handle.seek(0)
        self._header_bytes = bytearray()

        read_byte = self._file_handle.read(1)
        if read_byte != b'{':
            raise BinaryRecordsError('Not a Binary Records file, JSON header not found at first byte.',
                                     file_path=self._file_path)

        level = 0
        while read_byte:
            self._header_bytes.append(ord(read_byte))
            if read_byte == b'{':
                level += 1
            elif read_byte == b'}':
                level -= 1

            if level == 0:
                break
            elif len(self._header_bytes) >= 1000:
                raise BinaryRecordsError('Could not find end of JSON header before 1000 byte header limit.',
                                         file_path=self._file_path)

            # read next byte
            read_byte = self._file_handle.read(1)

        if level != 0:
            raise BinaryRecordsError('Could not find end of JSON header before end of file.',
                                     file_path=self._file_path)

        self._data_start_byte = self._file_handle.tell()

    def __iter__(self):
        return self

    def __next__(self):
        return_rec = self._read_record()
        if not return_rec:
            raise StopIteration
        else:
            return return_rec

    def get_rec_types(self):
        return [int(key) for key in self._header['rec_formats']]

    def get_rec_formats(self):
        return {int(key): value for key, value in self._header['rec_formats'].items()}

    def get_rec_labels(self):
        return {int(key): value for key, value in self._header['rec_labels'].items()}

    def _read_record(self):
        # Assuming file_handle pointer is aligned to the beginning of a message
        # read header
        rec_head_bytes = self._file_handle.read(5)
        if not rec_head_bytes:
            return None

        try:
            rec_ind, rec_type_id = struct.unpack('=IB', rec_head_bytes)

            rec_fmt = self._header['rec_formats'][str(rec_type_id)]
            rec_data_bytes = self._file_handle.read(struct.calcsize('='+rec_fmt))
            rec_data = struct.unpack('='+rec_fmt, rec_data_bytes)
        except struct.error as ex:
            raise BinaryRecordsError('File might be corrupted, record does not match format or unexpected EOF.',
                                     file_path=self._file_path)

        return rec_ind, rec_type_id, rec_data

    def convert_pandas(self):
        # always return to start of data
        self._file_handle.seek(self._data_start_byte)

        columns = self.get_rec_labels()
        rec_data = {key: [] for key in columns.keys()}

        rec_count = 0
        for rec in self:
            rec_data[rec[1]].append((rec[0],) + rec[2])

        panda_frames = {key: pd.DataFrame(data=rec_data[key], columns=['rec_ind'] + columns[key])
                        for key in columns.keys()}

        return panda_frames



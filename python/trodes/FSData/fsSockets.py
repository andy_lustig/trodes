# fsSockets.py: Shared code for messaging between fsData and fsGUI / trodes
#
# Copyright 2015 Loren M. Frank
#
# This program is part of the trodes data acquisition package.
# trodes is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# trodes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
#
import logging

import socket
import struct

from trodes.FSData.datatypes import SpikePoint, LFPPoint, PosPoint, DigIOStateChange, SystemTimePoint

import sys
sys.path.append('../Modules/FSGui/')
import fsShared

try:
    from IPython.terminal.debugger import TerminalPdb
    bp = TerminalPdb(color_scheme='linux').set_trace
except AttributeError as err:
    print('Warning: Attribute Error ({}), disabling IPython TerminalPdb.'.format(err))
    bp = lambda: None


# import trodes.FSData.fsShared as fsShared

class FSGuiMessage:
    def __init__(self, msg_type, data_len, msg_data):
        self.msg_type = msg_type
        self.data_len = data_len
        self.msg_data = msg_data


class TrodesDataMessage:
    def __init__(self, msg_type, data_len, msg_data, ntrode_index):
        self.msg_type = msg_type
        self.data_len = data_len
        self.msg_data = msg_data
        self.ntrode_index = ntrode_index
        self.data = self._unpack_msg_data(self.msg_data)

    def _unpack_msg_data(self, msg_data):
        if self.msg_type == fsShared.TRODESDATATYPE_CONTINUOUS:
            timestamp, cont_data, tv_sec, tv_nsec = struct.unpack_from('=Ihqq', msg_data)
            return (LFPPoint(timestamp=timestamp, ntrode_index=self.ntrode_index, data=cont_data),
                    SystemTimePoint(timestamp=timestamp, tv_sec=tv_sec, tv_nsec=tv_nsec))

        elif self.msg_type == fsShared.TRODESDATATYPE_SPIKES:
            # is there a better way to do this?
            parsestring = '=' + str(int((len(msg_data) - 4) / 2)) + 'h'
            timestamp = struct.unpack_from('=I', msg_data)[0]
            spike_data = struct.unpack_from(parsestring, msg_data, 4)
            return SpikePoint(timestamp=timestamp, ntrode_index=self.ntrode_index, data=spike_data)
            # note that due to packing, this could have extra elements, so the length should be checked when
            # the data are used

        elif self.msg_type == fsShared.TRODESDATATYPE_POSITION:
            timestamp, pos_x, pos_y, camera_id = struct.unpack_from('=IHHB', msg_data)
            return PosPoint(timestamp=timestamp, x=pos_x, y=pos_y, camera_id=camera_id)

        elif self.msg_type == fsShared.TRODESDATATYPE_DIGITALIO:
            timestamp, port, io_dir, state = struct.unpack_from('=Iicc', msg_data)
            return DigIOStateChange(timestamp=timestamp, port=port, io_dir=io_dir, state=state)


class FSUDPSocket:
    def __init__(self, hostname, port):

        self.class_log = logging.getLogger(name='{}.{}'.format(self.__class__.__module__, self.__class__.__name__))

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.connect((hostname, port))
        self.dataclientinfo = []
        self._fileno = self.sock.fileno()

    def get_local_port(self):
        # for UDP sockets, the 2nd entry of the name should be the port
        return self.sock.getsockname()[1]

    def send_message(self, msg_type, data):
        # pack the message type and length and then add the data, which must be a bytearray
        packeddata = struct.pack('=BI', msg_type, len(data))
        packeddata += data
        datalen = len(packeddata)
        totalsent = 0
        totalsent = self.sock.sendall(packeddata)
        return totalsent

    def get_message(self):
        # get the message all at once; otherwise we lose data (at least in OS X)
        data = self.sock.recv(1024)
        if len(data) == 0:
            self.class_log.warning('zero length message received')
            return None
        minfo = struct.unpack_from('=BI', data)

        return minfo[0], minfo[1], data[5:]

    def fileno(self):
        return self._fileno


class FSTCPSocket:
    def __init__(self, hostname, port):

        self.class_log = logging.getLogger(name='{}.{}'.format(self.__class__.__module__, self.__class__.__name__))

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.sock.connect((hostname, port))
        except:
            self.class_log.error('Error connecting socket to {} port {}'.format(hostname, port), exc_info=True)
        self.dataclientinfo = []
        self._fileno = self.sock.fileno()

    def get_local_port(self):
        # for TCP sockets, the 2nd entry of the name should be the port
        return self.sock.getsockname()[1]

    def send_message(self, msg_type, data):
        # pack the message type and length and then add the data, which must be a bytearray
        packeddata = struct.pack('=BI', msg_type, len(data))
        packeddata += data
        datalen = len(packeddata)
        totalsent = 0
        totalsent = self.sock.send(packeddata)
        return totalsent

    def get_message(self):
        # get the message type and the size
        data = self.sock.recv(5)
        if len(data) == 0:
            self.class_log.warning('zero length message received')
            return None
        minfo = struct.unpack_from('=BI', data)

        toread = minfo[1]
        data = bytearray()
        while toread > 0:
            chunk = self.sock.recv(toread)
            if chunk == '':
                raise RuntimeError("socket connection broken")
            data.extend(chunk)
            toread -= len(chunk)

        return minfo[0], minfo[1], data

    def fileno(self):
        return self._fileno


class FSGuiSocketHandler:

    def __init__(self, fsgui_socket):
        self.fs_socket = fsgui_socket
        self._fileno = self.fs_socket.fileno()

    def get_fsgui_message(self):
        msg_type, msg_data_len, msg_data = self.fs_socket.get_message()
        return FSGuiMessage(msg_type=msg_type, data_len=msg_data_len, msg_data=msg_data)

    def send_fsgui_message(self, message_type, data):
        return self.fs_socket.send_message(msg_type=message_type, data=data)

    def get_raw_socket(self):
        return self.fs_socket.sock

    def fileno(self):
        return self._fileno


class TrodesDataSocketHandler:

    def __init__(self, data_client_info, trodes_data_socket):
        self.data_client_info = data_client_info
        self.fs_socket = trodes_data_socket
        self._fileno = self.fs_socket.fileno()

    def get_data_message(self):
        msg_type, msg_data_len, msg_data = self.fs_socket.get_message()
        return TrodesDataMessage(msg_type=msg_type, data_len=msg_data_len, msg_data=msg_data,
                                 ntrode_index=self.data_client_info.nTrodeIndex)

    def send_trodes_message(self, msg_type, data):
        self.fs_socket.send_message(msg_type=msg_type, data=data)

    def get_raw_socket(self):
        return self.fs_socket.sock

    def fileno(self):
        return self._fileno


class HardwareSocket:
    """"socket for connection to MCU hardware"""

    def __init__(self, hostname, port):
        self.class_log = logging.getLogger(name='{}.{}'.format(self.__class__.__module__, self.__class__.__name__))
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            self.hostname = hostname
            self.port = port
            #self.sock.bind((hostName, port))  # is this necessary?
            self.sock.bind(('0.0.0.0', port))
            self.sock.connect((hostname, port))
        except:
            self.class_log.error('Failed creating MCU/ECU socket ({:}, {:})'.format(hostname, port))
            #self.sock = None
            raise

    def senddata(self, data):
        self.sock.sendall(data)


class ECUSocket(HardwareSocket):

    def __init__(self, hostname, port):
        super().__init__(hostname, port)

    def send_shortcut_message(self, function_num):
        # print('FSData: Sending ECU Shortcut ', function_num)
        data = struct.pack('=H', function_num)
        self.senddata(data)


class MCUSocket(HardwareSocket):

    def __init__(self, hostname, port):
        super().__init__(hostname, port)

    def send_settle_command(self):
        command_code = 102
        data = struct.pack('=B', command_code)
        self.senddata(data)


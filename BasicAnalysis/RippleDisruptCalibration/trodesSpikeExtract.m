function out = trodesSpikeExtract(filename, channels, skipTime, varargin)
%out = trodesSpikeExtract(filename,channels,refChan)
%out = trodesSpikeExtract(filename,channels,refChan,skipTime)
%out = trodesSpikeExtract(filename,channels,refChan,skipTime,OPTIONS)
%Reads in the designated channels from a Trodes .rec file
%filename -- the name of the .rec file, i.e., 'myrecording.rec'
%channels -- a list of channels to extract from the file, where each entry
%row lists the nTrode ID, follwed by the channel number (1-based)
%downSampledRate -- rate to downsample data to before filtering
%skipTime (default 0) -- skips reading in the time of each sample
%out -- a structure containing the broadband traces of the channels,timestamps, and some configuration info
%OPTIONS
%configFileName -- if the configuration settings at not at the top of the
%file, you can designate another file which has the config settings.

if (nargin < 4)
    skipTime = 0;
end

cutoff_low = 600;
cutoff_high = 6000;
filt_order = 4;


cont_data = readTrodesFileContinuous(filename, channels, skipTime, varargin);

fs = cont_data.samplingRate;


% design filter
[b_an,a_an] = besself(filt_order,[2*pi*cutoff_low,2*pi*cutoff_high]);
[b,a] = bilinear(b_an,a_an,fs);

spike_filter_data = filter(b,a,cont_data.channelData);

samples_per_ms = 30;

win_size = 4;           %ms
win_center = 1;         %ms
threshold = 50/(38/200);         %uV
refractory = 1*samples_per_ms;
samples_behind = win_center*samples_per_ms;
samples_ahead = (win_size-win_center)*samples_per_ms;

spk_thresh = find(spike_filter_data > threshold);
spktime_ind = [1 (find(diff(spk_thresh) > refractory) + 1)'];

spktimes = spk_thresh(spktime_ind);

spk_ind = cell2mat(arrayfun(@(x)(x-samples_behind:x+samples_ahead-1),spktimes,'UniformOutput',false));

spk_trans = spike_filter_data';
waves = spk_trans(spk_ind);
    
keyboard;

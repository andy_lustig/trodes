#include "datexporthandler.h"
#include <algorithm>
#include <iostream>

DATExportHandler::DATExportHandler(QStringList arguments):
    AbstractExportHandler(arguments)
{

    parseArguments();

    /*
    Do custom argument checks here like this:
    if (ARGVAL != REQUIREMENT) {
        qDebug() << "Error: ....";
        argumentReadOk = false;
    }   
    */

    //Parse custom arguments
    //parseCustomArguments(argumentsProcessed);

    if (argumentsProcessed != argumentList.length()-1) {
        _argumentsSupported = false;
        return;
    }
}

DATExportHandler::~DATExportHandler()
{

}

void DATExportHandler::printHelpMenu() {
    //printf("-outputrate <integer>  -- The sampling rate of the output file. \n"
    //       );

    printf("\nUsed to extract data from a raw rec file and save to a set of files in MDA format. \n");
    printf("NOTE: FILTERING IS TURNED OFF. \n");
    printf("Usage:  exportmda -rec INPUTFILENAME OPTION1 VALUE1 OPTION2 VALUE2 ...  \n\n"
           "Input arguments \n");
    printf("Defaults:\n -outputrate -1 (full) \n -usespikefilters 1 \n -interp -1 (inf) \n -userefs 1 \n");

    //AbstractExportHandler::printHelpMenu();
}

void DATExportHandler::parseArguments() {
    //Parse extra arguments not handled by the base class

    int optionInd = 1;
    while (optionInd < argumentList.length()) {

        if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            //printCustomMenu();
            //return;
            printHelpMenu();
        }
        optionInd++;
    }

    AbstractExportHandler::parseArguments();
}


int DATExportHandler::processData() {
    qDebug() << "Exporting to MDA format file...";

    //Calculate the packet positions for each channel that we are extracting, plus
    //other critical info (number of saved channels, reference info, etc).
    calculateChannelInfo();
    createFilters();

    qint32 numPointsWrittenToFile = 0;

    if (!openInputFile()) {
        return -1;
    }


    //Create a directory for the output files located in the same place as the source file
    QFileInfo fi(recFileName);
    QString fileBaseName;
    if (outputFileName.isEmpty()) {
        fileBaseName = fi.baseName();
    } else {
        fileBaseName = outputFileName;
    }

    QString directory;
    if(outputDirectory.isEmpty()) {
        directory = fi.absolutePath();
    } else {
        directory = outputDirectory;
    }

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+".mda"+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating MDA directory.";
            return -1;
        }
    }

    QList<QFile*> neuroFilePtrs;
    QList<QDataStream*> neuroStreamPtrs;

    QList<QFile*> timeFilePtrs;
    QList<QDataStream*> timeStreamPtrs;



    //Create an output file for the timestamps
    //*****************************************
    timeFilePtrs.push_back(new QFile);
    timeFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".timestamps.mda"));
    if (!timeFilePtrs.last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating output file.";
        return -1;
    }
    timeStreamPtrs.push_back(new QDataStream(timeFilePtrs.last()));
    timeStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

    //Write the current settings to file
    qint32 tDataType = -8; //-8 now means uint32 datatype
    qint32 tByteSize = 2; //Redundant
    qint32 tArrayDim = 1;  //The dataset will be M by 1, which is 1-dim
    qint32 tDim1 = 0; //This is the M component, which we don't know yet.

    *timeStreamPtrs.last() << tDataType << tByteSize << tArrayDim << tDim1;


    //Create an output file for the neural data
    //*****************************************

    //Create the output files (one for each nTrode) and write header sections
    for (int i=0; i<spikeConf->ntrodes.length(); i++) {
        neuroFilePtrs.push_back(new QFile);

        neuroFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".nt%1.mda").arg(spikeConf->ntrodes[i]->nTrodeId));
        if (!neuroFilePtrs.last()->open(QIODevice::WriteOnly)) {
            qDebug() << "Error creating output file.";
            return -1;
        }
        neuroStreamPtrs.push_back(new QDataStream(neuroFilePtrs.last()));
        neuroStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

        //Write the current settings to file
        qint32 dataType = -4; //-4 means int16 datatype
        qint32 byteSize = 2; //Redundant
        qint32 arrayDim = 2;  //The dataset will be M by N, which is 2-dim
        qint32 dim1 = spikeConf->ntrodes[i]->hw_chan.length(); // num channels in nTrode
        qint32 dim2 = 0;  //This is the N component, which we don't know yet.


        *neuroStreamPtrs.last() << dataType << byteSize << arrayDim << dim1 << dim2;

        //neuroFilePtrs.last()->flush();
    }


    //************************************************

    int inputFileInd = 0;

    while (inputFileInd < recFileNameList.length()) {
        if (inputFileInd > 0) {
            //There are multiple files that need to be stiched together. It is assumed that they all have
            //the exact same header section.
            recFileName = recFileNameList.at(inputFileInd);
            uint32_t lastFileTStamp = currentTimeStamp;

            qDebug() << "\nAppending from file: " << recFileName;
            QFileInfo fi(recFileName);

            if (!fi.exists()) {
                qDebug() << "File could not be found: " << recFileName;
                break;
            }
            if (!openInputFile()) {
                qDebug() << "Error: it appears that the file does not have an identical header to the last file. Cannot append to file.";
                return -1;
            }
            for (int i=0; i < channelFilters.length(); i++) {
                channelFilters[i]->resetHistory();
            }
            if (currentTimeStamp < lastFileTStamp) {
                qDebug() << "Error: timestamps do not begin with greater value than the end of the last file. Aborting.";
                return -1;
            }

        }



        //Process the data and stream results to output files
        while(!filePtr->atEnd()) {
            //Read in a packet of data to make sure everything looks good
            if (!(filePtr->read(buffer.data(),filePacketSize) == filePacketSize)) {
                //We have reached the end of the file
                break;
            }
            //Find the time stamp
            bufferPtr = buffer.data()+packetTimeLocation;
            tPtr = (uint32_t *)(bufferPtr);
            currentTimeStamp = *tPtr+startOffsetTime;

            //The number of expected data points between this time stamp and the last (1 if no data is missing)
            int numberOfPointsToProcess = (currentTimeStamp-lastTimeStamp);
            if (numberOfPointsToProcess < 1) {
                qDebug() << "Error with file: backwards timestamp at time " << currentTimeStamp << lastTimeStamp;
                if (abortOnBackwardsTimestamp) {
                    qDebug() << "Aborting";
                    return -1;
                } else {
                    for (int i=0; i < channelFilters.length(); i++) {
                        channelFilters[i]->resetHistory();
                    }


                    numberOfPointsToProcess = 1;
                    pointsSinceLastLog = -1;
                }
            }

            if ((maxGapSizeInterpolation > -1) && (numberOfPointsToProcess > (maxGapSizeInterpolation+1))) {
                //The gap in data is too large to interpolate over, so we reset the filters (maxGapSizeInterpolation of -1 means the no gap is too large)
                for (int i=0; i < channelFilters.length(); i++) {
                    channelFilters[i]->resetHistory();
                }
                numberOfPointsToProcess = 1;
                pointsSinceLastLog = -1;
            }

            if (numberOfPointsToProcess == 1) {
                //No missing data
                if (((pointsSinceLastLog+1)%decimation) == 0) {
                    *timeStreamPtrs.at(0) << currentTimeStamp;
                    numPointsWrittenToFile++;
                }
            } else {
                //Some missing data, which we will interpolate over
                for (int p=1; p<numberOfPointsToProcess+1; p++) {
                    if (((pointsSinceLastLog+p)%decimation) == 0) {
                        *timeStreamPtrs.at(0) << lastTimeStamp+p;
                        numPointsWrittenToFile++;
                    }
                }
            }
            for (int chInd=0; chInd < channelPacketLocations.length(); chInd++) {
                bufferPtr = buffer.data()+channelPacketLocations[chInd];
                tempDataPtr = (int16_t*)(bufferPtr);
                tempDataPoint = *tempDataPtr;

                if (useRefs && refOn[chInd]) {
                    //Subtract the digital reference
                    bufferPtr = buffer.data() + refPacketLocations[chInd];
                    tempDataPtr = (int16_t*)(bufferPtr);
                    tempRefPoint = *tempDataPtr;
                    tempDataPoint -= tempRefPoint;
                }



                if (numberOfPointsToProcess == 1) {
                    //No missing data


                    /*
                    if (filterOn[chInd]) {
                        tempFilteredDataPoint = channelFilters.at(chInd)->addValue(tempDataPoint);
                    }*/

                    if (((pointsSinceLastLog+1)%decimation) == 0) {
                        *neuroStreamPtrs.at(nTrodeIndForChannels[chInd]) << tempDataPoint;


                        /*
                        if (filterOn[chInd]) {
                            //*neuroStreamPtrs.at(chInd) << tempFilteredDataPoint; //1 file per channel
                            *neuroStreamPtrs.at(0) << tempFilteredDataPoint; //all channels in one file


                        } else {
                            //*neuroStreamPtrs.at(chInd) << tempDataPoint; //1 file per channel
                            *neuroStreamPtrs.at(0) << tempFilteredDataPoint; //all channels in one file
                        }*/
                    }

                } else {
                    //We have missing data, so we need to interpolate

                    int16_t interpolatedDataPtInt;
                    double interpolatedDataPtFloat;
                    for (int mp = 1; mp < numberOfPointsToProcess+1; mp++) {
                        interpolatedDataPtFloat = (double)dataLastTimePoint[chInd] + ((double)(mp)/numberOfPointsToProcess)*(tempDataPoint-dataLastTimePoint[chInd]);
                        interpolatedDataPtInt = round(interpolatedDataPtFloat);
                        /*
                        if (filterOn[chInd]) {
                            interpolatedDataPtInt = channelFilters.at(chInd)->addValue(interpolatedDataPtInt);
                        }*/

                        if (((pointsSinceLastLog+mp)%decimation) == 0) {

                            *neuroStreamPtrs.at(nTrodeIndForChannels[chInd]) << interpolatedDataPtInt;

                        }

                    }

                }

                dataLastTimePoint[chInd] = tempDataPoint; //remember the last unfilted data point
            }

            //Print the progress to stdout
            printProgress();

            lastTimeStamp = currentTimeStamp;
            pointsSinceLastLog = (pointsSinceLastLog+numberOfPointsToProcess)%decimation;


        }

        printf("\rDone\n");
        filePtr->close();
        inputFileInd++;
    }

    for (int i=0; i < neuroFilePtrs.length(); i++) {
        neuroFilePtrs[i]->flush();
        neuroFilePtrs[i]->seek(16);
        *neuroStreamPtrs.at(i) << numPointsWrittenToFile;

        neuroFilePtrs[i]->close();
    }

    for (int i=0; i < timeFilePtrs.length(); i++) {
        timeFilePtrs[i]->flush();
        timeFilePtrs[i]->seek(12);
        *timeStreamPtrs.at(i) << numPointsWrittenToFile;

        timeFilePtrs[i]->close();
    }



    return 0; //success
}

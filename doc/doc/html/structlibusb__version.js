var structlibusb__version =
[
    [ "describe", "structlibusb__version.html#af42453a3421515fa2db3fdf30f2367c3", null ],
    [ "major", "structlibusb__version.html#a7f80ed3e95566b639fa09380e94f8d59", null ],
    [ "micro", "structlibusb__version.html#a6a73a21564237c1fa0e6e5b038e98605", null ],
    [ "minor", "structlibusb__version.html#ac886b798704bb9ca2b6cd3ae0234591f", null ],
    [ "nano", "structlibusb__version.html#afbe4cf431bea706294667e333878508c", null ],
    [ "rc", "structlibusb__version.html#a99937642131c0559025bad92e7f51f27", null ]
];
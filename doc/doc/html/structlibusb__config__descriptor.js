var structlibusb__config__descriptor =
[
    [ "bConfigurationValue", "structlibusb__config__descriptor.html#a04549d99d38080857e0256fcad8dcce7", null ],
    [ "bDescriptorType", "structlibusb__config__descriptor.html#abc4f9a751c5b2ed968b045cb17528f74", null ],
    [ "bLength", "structlibusb__config__descriptor.html#a18ccd46a86328fd9c849e17ca8f01738", null ],
    [ "bmAttributes", "structlibusb__config__descriptor.html#adb65fe86ce28394110caad982c566525", null ],
    [ "bNumInterfaces", "structlibusb__config__descriptor.html#a2c89b2d0a9ec0440801b97a8f8145792", null ],
    [ "extra", "structlibusb__config__descriptor.html#acc982e6c82f7183dfa41076d28d991f7", null ],
    [ "extra_length", "structlibusb__config__descriptor.html#a18439e83e8f46606a13d70c00a48a9d9", null ],
    [ "iConfiguration", "structlibusb__config__descriptor.html#a06dbb2e068b3999dd23954e63640d953", null ],
    [ "interface", "structlibusb__config__descriptor.html#afe04b70611b0ac83866639103d27df76", null ],
    [ "MaxPower", "structlibusb__config__descriptor.html#a527518147a0c7321a195f2d8dac2ea01", null ],
    [ "wTotalLength", "structlibusb__config__descriptor.html#a819c160c66c1bd7b569399e6a6fc278f", null ]
];
var structlibusb__interface__descriptor =
[
    [ "bAlternateSetting", "structlibusb__interface__descriptor.html#a2c94bce217e8ecd5fd23728510e9c838", null ],
    [ "bDescriptorType", "structlibusb__interface__descriptor.html#affe977ae83d3bbf07513d87ebfb6ed0e", null ],
    [ "bInterfaceClass", "structlibusb__interface__descriptor.html#a19dcf80e6a5fedf3d3673630897ad649", null ],
    [ "bInterfaceNumber", "structlibusb__interface__descriptor.html#a75046f443d3330f590f3d3fb1f9df863", null ],
    [ "bInterfaceProtocol", "structlibusb__interface__descriptor.html#a5062bbb17fc76e6c3624ea7ca68d554c", null ],
    [ "bInterfaceSubClass", "structlibusb__interface__descriptor.html#a0b9b890f84694a4e6a0103944e1247f4", null ],
    [ "bLength", "structlibusb__interface__descriptor.html#a646b7bf875e5b927796d47e57c9182b8", null ],
    [ "bNumEndpoints", "structlibusb__interface__descriptor.html#a487bd2058bfbe749d370c6e7d3add20e", null ],
    [ "endpoint", "structlibusb__interface__descriptor.html#a42bd0c63b361cf6c0bda5ecdf73e1c75", null ],
    [ "extra", "structlibusb__interface__descriptor.html#aa1169e8c0b22a9928bbd51169609f31d", null ],
    [ "extra_length", "structlibusb__interface__descriptor.html#ad77fa0b951a83d0c7eac28920367f200", null ],
    [ "iInterface", "structlibusb__interface__descriptor.html#a19227cc111c27e27c2c07430777e1ab8", null ]
];
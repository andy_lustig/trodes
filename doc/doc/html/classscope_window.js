var classscope_window =
[
    [ "scopeWindow", "classscope_window.html#a5551eff7ab4cd796bd4c834380482502", null ],
    [ "~scopeWindow", "classscope_window.html#a422651f26ab226e0e38bb46a565b84b1", null ],
    [ "clicked", "classscope_window.html#a0bdfe931b008ea44f3f73fd2319305fc", null ],
    [ "mousePressEvent", "classscope_window.html#ae594135f911ce311c874727c52ccc129", null ],
    [ "paintEvent", "classscope_window.html#a768da726d7964fa482730f86af86ceed", null ],
    [ "plotData", "classscope_window.html#ad52aafb262f1bf9beaf55e091685c8cd", null ],
    [ "setHighlight", "classscope_window.html#a0a7f419c407bc7a2e0adb5ea82126358", null ],
    [ "setMaxDisplay", "classscope_window.html#aed73605a0fb2dea4b2a74a63f0f9a557", null ],
    [ "setThresh", "classscope_window.html#ab9bbb2db131fcb9e2f188a0810b57d18", null ],
    [ "afterTriggerLength", "classscope_window.html#ae90147fa1d5ef21a943bee3608b339b6", null ],
    [ "beforeTriggerLength", "classscope_window.html#a989002e7f3d92add19535f532b12c47c", null ],
    [ "highlight", "classscope_window.html#a037e79c646dbbb37d5e59a6fbdeb0041", null ],
    [ "maxAmplitude", "classscope_window.html#af2f6bb929ac1e0d36acf28a41f4fe3a1", null ],
    [ "nTrodeChannel", "classscope_window.html#ae6a32ccb993fa82eabbcb1944d600448", null ],
    [ "thresh", "classscope_window.html#addb2b51ddf511c48bec2693cd02b50c4", null ],
    [ "traceColor", "classscope_window.html#abb905c149833f8176a93fabe0df9bd8e", null ],
    [ "waveFormData", "classscope_window.html#a1d2228a9477e7ece7b7e9c8d2b672993", null ]
];
var classsource_controller =
[
    [ "sourceController", "classsource_controller.html#aa48c3c45db4d4333678c82d79e4dd9fa", null ],
    [ "acquisitionPaused", "classsource_controller.html#a56f0ed1fbcc83c8a8d6b595fdf6ae35e", null ],
    [ "acquisitionStarted", "classsource_controller.html#a046c66aa65e361294127edf1af78ca23", null ],
    [ "acquisitionStopped", "classsource_controller.html#ad608adf9dc90c5bc1b87230f59a29bc5", null ],
    [ "clearBuffers", "classsource_controller.html#ad331da31c8fb9d54061cdcdd8f6ce91d", null ],
    [ "connectToSource", "classsource_controller.html#a8245034cd173f15e6cdd34cebe5afb1e", null ],
    [ "disconnectFromSource", "classsource_controller.html#a7ccdefb6fc7773e690107854aa0fc579", null ],
    [ "PauseAcquisition", "classsource_controller.html#afbcb3b260ae5c0b7ef7ac2ff0fb21774", null ],
    [ "pauseSource", "classsource_controller.html#ad9f0da762239cef530e4b5b75d3e9d14", null ],
    [ "setSource", "classsource_controller.html#a3b671c721bfe461c2b432a0222875ad5", null ],
    [ "setSourceState", "classsource_controller.html#a53c9cf87ff17a7fe341d15794fa20a2c", null ],
    [ "StartAcquisition", "classsource_controller.html#ac10d7742c4b15e2bfc3bd6ccccf76386", null ],
    [ "stateChanged", "classsource_controller.html#af0f29da878b67ab3d43b53591a1545b4", null ],
    [ "StopAcquisition", "classsource_controller.html#a0e2868eda584be1ef9fc8c33a462b0d1", null ],
    [ "currentSource", "classsource_controller.html#ae49d1748da66b1232d38fdaf84048355", null ],
    [ "fileSource", "classsource_controller.html#af773e6ab2708d2ed72947734921293f4", null ],
    [ "state", "classsource_controller.html#aba8d41dc2a1b3ab28191fe29161c5727", null ],
    [ "USBSource", "classsource_controller.html#a47551d22b0d425dcc303e89583df7ff0", null ],
    [ "waveGeneratorSource", "classsource_controller.html#ac9447a378418fb29e099f1b8fcb84c70", null ]
];
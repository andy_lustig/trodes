var classtrigger_scope_settings_widget =
[
    [ "triggerScopeSettingsWidget", "classtrigger_scope_settings_widget.html#a02c2c753a657d9a0e5f03e31d7932da2", null ],
    [ "~triggerScopeSettingsWidget", "classtrigger_scope_settings_widget.html#af234b461d0b610cd80b16d675d27c6dc", null ],
    [ "changeAllFilters", "classtrigger_scope_settings_widget.html#aab4b3119e083af3fe4d4f0e4ca2bb8a3", null ],
    [ "changeAllRefs", "classtrigger_scope_settings_widget.html#a7536794820b0fec7b694d660a967b1b9", null ],
    [ "linkBoxClicked", "classtrigger_scope_settings_widget.html#a8d8d6247530e35d3627840ff4597b16b", null ],
    [ "toggleAllFilters", "classtrigger_scope_settings_widget.html#a0b3ae94e425a6dd4156b9f96e0c7b42c", null ],
    [ "toggleAllRefs", "classtrigger_scope_settings_widget.html#aebd4a1bc69093339b982b1151bc27469", null ],
    [ "toggleLinkChanges", "classtrigger_scope_settings_widget.html#aba7e633d1c80c851ffa919b955e7f97e", null ],
    [ "updateAudioSettings", "classtrigger_scope_settings_widget.html#af38e3ddd1ae546026ec572e3d9407107", null ],
    [ "updateFilterSwitch", "classtrigger_scope_settings_widget.html#a34efe1a46d8bd94e8bbe590520a24e36", null ],
    [ "updateLfpChanel", "classtrigger_scope_settings_widget.html#abaad208e7ca94c31b2971b971e17e430", null ],
    [ "updateLfpSwitch", "classtrigger_scope_settings_widget.html#a419d0634a5a31dbf62058f37fde1a9fc", null ],
    [ "updateLfpUpperFilter", "classtrigger_scope_settings_widget.html#a3351d97c4b84638cb76616690266e1bd", null ],
    [ "updateLowerFilter", "classtrigger_scope_settings_widget.html#a7e06ce5b321827a5cfde6d53bf39296d", null ],
    [ "updateRefChan", "classtrigger_scope_settings_widget.html#a0aada1ce1cfef4d4c02c17072b4bd272", null ],
    [ "updateRefSwitch", "classtrigger_scope_settings_widget.html#af97a5dfd1aedbc5737f5503c79dc68f3", null ],
    [ "updateRefTrode", "classtrigger_scope_settings_widget.html#ad95082fce2b39c3b6f7528e272e988d8", null ],
    [ "updateUpperFilter", "classtrigger_scope_settings_widget.html#ab3fa330cd4a204773e80ab5b85c19059", null ],
    [ "filterBox", "classtrigger_scope_settings_widget.html#a004c8d53d8a8d3986003711623485259", null ],
    [ "highFilterCutoffBox", "classtrigger_scope_settings_widget.html#a5a226f1bcf08d8cc0b0e7eb436e93fb6", null ],
    [ "labels", "classtrigger_scope_settings_widget.html#a1516e5a6d5c7b315721e38931be9b37b", null ],
    [ "LFPChannelBox", "classtrigger_scope_settings_widget.html#a56b1f43a5d5e19560d0f999c32ae6e1c", null ],
    [ "LFPFilterBox", "classtrigger_scope_settings_widget.html#abe2a9167a80e5c35567fca31dcfab310", null ],
    [ "LFPHighFilterCutoffBox", "classtrigger_scope_settings_widget.html#ab5e93416b5913085d93a419cc9230e57", null ],
    [ "linkCheckBox", "classtrigger_scope_settings_widget.html#a74088a94173b3e6061a65b74795866a4", null ],
    [ "lowFilterCutoffBox", "classtrigger_scope_settings_widget.html#a1f4fcf3caf1d629fe4f865700dacaa93", null ],
    [ "nTrodeNumber", "classtrigger_scope_settings_widget.html#ad3479a49a4b92c7f952ae14b4d8258e9", null ],
    [ "refBox", "classtrigger_scope_settings_widget.html#a9654a51c1e7d50c00b2e734e2b242b48", null ],
    [ "RefChannelBox", "classtrigger_scope_settings_widget.html#abe1d6015ce1c3ecc68351985cabbc88b", null ],
    [ "RefTrodeBox", "classtrigger_scope_settings_widget.html#a81d06fdb7325677be47e32d15d662b9f", null ],
    [ "widgetLayouts", "classtrigger_scope_settings_widget.html#afe811dabcc69e1c66813f93f68c7a9ab", null ]
];
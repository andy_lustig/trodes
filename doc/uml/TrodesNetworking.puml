@startuml

package SocketStuff {
    Interface TrodesSocketServer {
        --public--
        {abstract} TrodesSocket getNewConnection()
        --signals--
        {abstract} newConnection()
        --slots--
    }

    class TrodesTCPSocketServer -up-|> TrodesSocketServer
    class TrodesUDPSocketServer -up-|> TrodesSocketServer

    TrodesSocket <-- TrodesSocketServer: < creates
    abstract class TrodesSocket {
        --public--
        qint64 bytesAvailable()
        {abstract} read(char *, qint64)
        {abstract}write(char *, qint64)
        --signals--
        --slots--
        --private--
        QAbstractSocket socket
        quint16 local_port
        QHostAddress remote_ip
        quint16 remote_port
    }

    class TrodesTCPSocket -up-|> TrodesSocket
    class TrodesUDPSocket -up-|> TrodesSocket

    class TrodesTCPSocket {
        normally TCP
        read/write
        calls
    }

    class TrodesUDPSocket {
        uses datagrams
        to implement
        read/write
    --
    }
}

package DataHandlers {
    class TrodesDataServerHandler *-- TrodesSocket: has 1 >
    abstract class TrodesDataServerHandler {
        --public--
        TrodesDataServerHandler(TrodesSocket* socket)
        --signals--
        turnDataStreamOn()
        turnDataStreamOff()
        --slots--
        --protected--
        --private--
        bool streamOn
        TrodesSocket socket
    }

    class TrodesDataClientHandler *-- TrodesSocket: has 1 >
    abstract class TrodesDataClientHandler {
        --public--
        TrodesDataClientHandler(TrodesSocket* socket)
        --signals--
        --slots--
        startStreaming()
        stopStreaming()
        --protected--
        --private--
        TrodesSocket socket
    }

    class TrodesContDataServerHandler --|> TrodesDataServerHandler
    class TrodesContDataServerHandler {
        --public--
        TrodesDataServerHandler(TrodesSocket* socket, )
        --signals--
        newDecimation(int)
        --slots--
        sendData(quint16)
        --private--
        int decimation
    }

    class TrodesContDataClientHandler --|> TrodesDataClientHandler
    class TrodesContDataClientHandler {
        --public--
        readData()
        --signals--
        newDataAvaliable()
        --slots--
        setDecimation(int)
        --private--
    }
}

package DataServerStuff {
    TrodesDataServer *-- TrodesSocketServer: has 1 >
    abstract class TrodesDataServer {
        --public--
        TrodesDataServer(TrodesSocketServer* server)
        QList<TrodesDataServerHandler> getHandlerList()
        --signals--
        newDataProvided(DataTypeSpec *)
        --slots--
        --private slots--
        disconnectHandler()
        --private--
        TrodesSocketServer server
    }

    TrodesContDataServer *--- TrodesContDataServerHandler: creates many >
    TrodesContDataServer -up-|> TrodesDataServer
    class TrodesContDataServer {
        one per streamprocessor thread
        --public--
        TrodesContDataServer(TrodesSocketServer* server, QList<quin16> nTrodeList)
        QList<TrodesContDataServerHandler> getContHanderList()
        --signals--
        --slots--
        --private--
        QList<qint16> nTrodeList
    }
}

package DataClientStuff {
    TrodesDataClient <..left..> TrodesDataServer: sockets
    class TrodesDataClient {
        --public--
        QList<TrodesDataClientHandler> getHandlerList()
        --signals--
        --slots--
        createClientHandler(QHostAddress host, quint16 port)
        --private--
    }

    TrodesContDataClient *-- TrodesContDataClientHandler: creates many >
    TrodesContDataClient -up-|> TrodesDataClient
    class TrodesContDataClient {
        --public--
        QList<TrodesContDataClientHandler> getContHandlerList()
        --signals--
        --slots--
        newContClientHandler(QHostAddress host, quint16 port)
        --private--
    }
}



package TrodesMessagingStuff {

    class TrodesMessageServer {
        --public--
        --signals--
        ..Module Init..
        moduleIDSet()
        moduleNameSet()
        datatypeAvaliable()
        datatypeRemoved()
        ..messages..
        stateScriptToSend()
        timeRateRequested()
        currentStateRequested()
        ..cleanup..
        moduleDisconnected()
        --slots--
        openRecordingFile()
        closeRecordingFile()
        startRecording()
        stopRecording()
        --private--
        QTCPServer server
        QList<QTCPSocket> sockets
    }

    TrodesMessageClient <..left.> TrodesMessageServer: sockets
    class TrodesMessageClient {
        --public--
        --signals--
        recordingFileOpened()
        recordingFileClosed()
        recordingStarted()
        recordingStopped()
        --slots--
        ..initialization..
        setModuleID()
        setModuleName()
        addDataAvailable()
        removeDataAvailable()
        ..messages..
        sendStateScript()
        sendTimeRateRequest()
        sendCurrentStateRequest()
        ..cleanup..
        disconnect()
        --private--
        QTCPSocket socket
    }

    TrodesServerDataNetwork <.left.> TrodesMessageServer: signal/slots
    class TrodesServerDataNetwork {
        master keeper of datatypes registered
        --public--
        --signals--
        masterDataTypeListUpdated()
        --slots--
        registerDataType()
        removeDataType()
        broadcastDataTypes()

        --private--
        QList<DataTypeSpec> allDataTypes
    }

    TrodesClientDataNetwork <.left.> TrodesMessageClient: signal/slots
    class TrodesClientDataNetwork {
        registers local datatypes (produced)
        and syncs master list (can receive)
        --public--
        --signals--
        localDataTypeListUpdated()
        --slots--
        registerDataType()
        removeDataType()
        updateDataTypeMaster()
        --private--
        QList<DataTypeSpec> localDatatypes
        QList<DataTypeSpec> allDatatypes
    }
}

package MainTrodesStuff {

    MainTrodes *- StreamProcessorManager: has one >
    MainTrodes *-- TrodesServerDataNetwork: has one >
    MainTrodes *-- TrodesMessageServer: has one >
    class MainTrodes {
    --private--
    TrodesMessageServer server
    TrodesServerDataNetwork dataNet
    }

    StreamProcessorManager *-down- StreamProcessor: has many >
    class StreamProcessorManager {
    }

    StreamProcessor *---- TrodesContDataServer: has one >
    StreamProcessor <..> TrodesServerDataNetwork: register datatypes
    class StreamProcessor {
    }
}

package MyFavoriteModuleA {
    note top of ModuleMain
        Module that generates data
    end note
    ModuleMain *-- TrodesMessageClient: has 1 >
    ModuleMain *-- TrodesClientDataNetwork: has 1 >
    ModuleMain *-- TrodesDataServer: has 1 >
    class ModuleMain {
    --private--
    TrodesMessageClient client
    TrodesClientDataNetwork dataNet
    TrodesDataServer dataServer

    }
}


@enduml
